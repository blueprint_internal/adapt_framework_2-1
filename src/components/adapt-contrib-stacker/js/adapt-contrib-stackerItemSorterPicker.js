define(function(require) {

    var Backbone = require('backbone');
    var Adapt = require('coreJS/adapt');

    var StackerItemSorterPickerView = Backbone.View.extend({

        className: 'stacker-item-sorter-picker',

        events: {
            'click .stacker-item-sorter-picker-selector': 'onSelectorClicked'
        },

        initialize: function(options) {
            this.data = options.data;
            this.parentView = options.parentView;
            this.listenTo(Adapt, 'remove', this.remove);
            this.render();
        },

        render: function() {
            var template = Handlebars.templates[this.constructor.template];
            this.$el.html(template({availableIndexes: this.data}));
            _.defer(_.bind(this.postRender, this));
        },

        postRender: function() {
            this.$el.css('margin-top', -this.$el.height()/2).addClass('show');
        },

        onSelectorClicked: function(event) {
            event.preventDefault();
            var index = parseInt($(event.currentTarget).attr('data-index'));
            var newPosition = this.data[index];
            this.parentView.updatePosition(newPosition);
        }

    }, {
        template: 'stackerItemSorterPicker'
    });

    return StackerItemSorterPickerView;

});