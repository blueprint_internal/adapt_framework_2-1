/*
* Stacker
* License - https://github.com/adaptlearning/adapt_framework/blob/master/LICENSE
* Maintainers - Daryl Hedley
*/

define(function(require) {

    var QuestionView = require('coreViews/questionView');
    var Adapt = require('coreJS/adapt');
    var StackerItemView = require('components/adapt-contrib-stacker/js/adapt-contrib-stackerItem');

    var StackerView = QuestionView.extend({
    
        className: function(options) {
            return "component "
            + "question-component " 
            + this.model.get('_component')
            + "-component " + this.model.get('_id') 
            + " " + this.model.get('_classes')
            + " " + this.setVisibility()
            + " component-" + this.model.get('_layout')
            + " nth-child-" + this.model.get('_nthChild');
        },

        onEnabledChanged: function () {},

        setupQuestion: function() {
            // Check which type of component this is: sliding, ordering or both
            this.isSliding = this.model.get('_shouldUseSliding');
            this.isOrdering = this.model.get('_shouldUseOrdering');
            // Set _originalIndex on the items
            _.each(this.model.get('_items'), function(item, index) {
                item._originalIndex = index;
            });

            if (this.isOrdering) {

                // Randomise the _items
                this._items = _.shuffle(this.model.get('_items'));
            } else {
                this._items = this.model.get('_items');
            }

            /*QuestionView.prototype.preRender.apply(this, arguments);*/
        },

        onQuestionRendered: function() {
            this.renderItemViews();
            _.defer(_.bind(this.setReadyStatus, this));
        },

        renderItemViews: function() {

            this.childViews = [];
            _.each(this._items, function(item, index) {

                _.each(item._options, function(option, index) {
                    option._originalIndex = index;
                });
                
                if (this.isSliding) {
                    // Should randomise the _options
                    item._options = _.shuffle(item._options);

                }

                var childView = new StackerItemView({
                    data: item,
                    index: index,
                    itemsLength: this._items.length,
                    parentView: this
                });
                // Store child view for later
                this.childViews.push(childView);
                item._view = childView;

                this.$('.stacker-holder').append(childView.$el);

            }, this);

        },

        updateIndexes: function(fromIndex, toIndex) {
            // Remove index classes
            this.$('.stacker-item').removeClass('first-child last-child');
            // First shift the this._items in the array
            this._items = this.moveItemInArray(this._items, fromIndex, toIndex);

            // Now update each item view to have the correct data
            _.each(this._items, function(item, index) {
                // As this is already sorted use the iterator index
                item._view.index = index;
                // Update each individual item index
                item._view.$('.stacker-item-sorter-index').html(index + 1);
                if (index === 0) {
                    item._view.$el.addClass('first-child');
                }
                if (index === this._items.length - 1) {
                    item._view.$el.addClass('last-child');
                }

            }, this);
        },

        moveItemInArray: function(array, fromIndex, toIndex) {
            var element = array[fromIndex]
            array.splice(fromIndex, 1);
            array.splice(toIndex, 0, element);
            return array;
        },
        
        /**
        * to be implemented by subclass
        */
        // compulsory methods
        canSubmit: function() {
            return true;
        },

        isCorrect: function() {
            
            _.each(this._items, function(item, index) {
                // This component has a two stage completion if sliding and ordering are both enabled
                var isCorrectOrder = false;
                if (item._originalIndex === index) {
                    isCorrectOrder = true;
                }

                var isCorrectOption = false;
                _.each(item._options, function(option, index) {
                    // First find out if it's the correct one
                    if (option._isCorrect) {
                        if (index === item.stage) {
                            isCorrectOption = true;
                        }
                    }
                });

                // Marking is different based upon the component type

                if (!this.isSliding) {
                    isCorrectOption = true;
                }

                if (!this.isOrdering) {
                    isCorrectOrder = true;
                }

                if (isCorrectOrder && isCorrectOption) {
                    item.correct = true;
                    this.model.set('_isAtLeastOneCorrectSelection', true);
                } else {
                    item.correct = false;
                }

            }, this);

            var isCorrect = false;

            var hasIncorrectItem = _.findWhere(this._items, {correct: false});

            if (!hasIncorrectItem) {
                isCorrect = true;
            }

            return isCorrect;

        },

        setScore: function() {
            var questionWeight = this.model.get("_questionWeight");
            var answeredCorrectly = this.model.get('_isCorrect');
            var score = answeredCorrectly ? questionWeight : 0;
            this.model.set('_score', score);
        },

        showMarking: function() {
            _.each(this.childViews, function(childView) {
                childView.showMarking();
            })
        },

        isPartlyCorrect: function() {
            return this.model.get('_isAtLeastOneCorrectSelection');
        },

        showCorrectAnswer: function() {
            for (var i = 0; i < this.childViews.length; i++) {
                _.each(this.childViews, function(childView) {
                    if (childView.data._originalIndex === i) {
                        this.$('.stacker-holder').append(childView.$el);
                        childView.showCorrectText(childView.data);
                    }
                }, this)
            };

        },

        hideCorrectAnswer: function() {

            var usersAnswers = this.model.get('_userAnswer');

            for (var i = 0; i < this.childViews.length; i++) {

                _.each(this.childViews, function(childView) {
                    if (childView.data._originalIndex === usersAnswers[i]._originalIndex) {
                        this.$('.stacker-holder').append(childView.$el);
                        childView.showUserAnswerText(usersAnswers[i]);
                    }
                }, this);

            };

        },

        storeUserAnswer: function() {
            // Store the users current answers
            var userAnswer = [];

            _.each(this._items, function(item, index) {
                // Get the value of each items stage which is 
                // easy as each item view has a getState method
                var itemState = item._view.getState();
                item.stage = itemState.stage;


                // Should remove the _view attribute and build a new item to store
                var itemToStore = {
                    _options: item._options,
                    _originalIndex: item._originalIndex,
                    _stage: itemState.stage,
                    text: item.text
                };

                userAnswer.push(itemToStore);

            }, this);

            this.model.set('_userAnswer', userAnswer);
        }
        
    }, {
        _isQuestionType: true
    });
    
    Adapt.register('stacker', StackerView);

    return StackerView;

});