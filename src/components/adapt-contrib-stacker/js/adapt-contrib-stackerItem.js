define(function(require) {

    var Backbone = require('backbone');
    var Adapt = require('coreJS/adapt');
    var StackerItemSorterPickerView = require('components/adapt-contrib-stacker/js/adapt-contrib-stackerItemSorterPicker');

    var StackerItemView = Backbone.View.extend({

        className: 'stacker-item component-item',

        events: {
            'click .stacker-item-navigation-left': 'onLeftNavigationClicked',
            'click .stacker-item-navigation-right': 'onRightNavigationClicked',
            'click .stacker-item-sorter': 'onSorterClicked'
        },

        initialize: function(options) {
            this.options = options;
            this.listenTo(Adapt, 'update:indexes', this.onUpdateIndexes);
            this.listenTo(Adapt, 'remove', this.remove);
            this.listenTo(Adapt, 'device:resize', this.adjustTexts);
            this.setClassNames();
            this.setInitialState(options);
            this.setupListeners();
            this.render();
        },

        setClassNames: function() {
            var classNames = 'stacker-item-' + this.options.index;
            if (this.options.index === 0) {
                classNames += ' first-child';
            }
            if (this.options.index === this.options.itemsLength - 1) {
                classNames += ' last-child';
            }
            this.$el.addClass(classNames);
        },

        setInitialState: function(options) {
            this.index = options.index;
            this.data = options.data;
            this.data.index = this.index;
            this.parentView = options.parentView;
            this.itemsLength = options.itemsLength;
            this.stage = 0;
            this.optionsLength = this.data._options.length;
        },

        setupListeners: function() {
            this.listenTo(this.parentView, 'remove:pickerViews', this.onRemovePickerViews);
        },

        render: function() {
            var template = Handlebars.templates[this.constructor.template];
            this.$el.html(template(this.data));
            _.defer(_.bind(function() {
                this.postRender();
            }, this));
        }, 

        postRender: function() {
            this.adjustTexts();
        },

        adjustTexts: function() {
            // Get width of text items
            this.$itemTexts = this.$('.stacker-item-texts');
            var itemTextsHolder = this.$('.stacker-item-texts-holder');
            this.$itemText = this.$('.stacker-item-text');
            // Should use the holder width otherwise it creates a recursive
            this.textWidth = itemTextsHolder.innerWidth();
            this.$itemText.width(this.textWidth);
            // Set container to width * options
            this.$itemTexts.width(this.textWidth * this.optionsLength);

        },

        onLeftNavigationClicked: function(event) {
            event.preventDefault();
            if (!this.parentView.model.get('_isEnabled')) {
                return;
            }
            this.removePickerViews();
            this.navigateLeft();
        },

        navigateLeft: function() {
            if (this.stage != 0) {
                this.stage--;
            } else {
                this.stage = this.optionsLength - 1;
            }

            this.$itemTexts.prepend(this.$('.stacker-item-text').last());
            this.$itemTexts.css({marginLeft: -this.textWidth}).velocity({marginLeft: 0}, 600, 'easeInOutQuad');

        },

        onRightNavigationClicked: function(event) {
            event.preventDefault();
            if (!this.parentView.model.get('_isEnabled')) {
                return;
            }
            this.removePickerViews();
            this.navigateRight();
        },

        navigateRight: function() {
            if (this.stage < this.optionsLength-1) {
                this.stage++;
            } else {
                this.stage = 0;
            }

            this.$itemTexts.velocity({marginLeft: -this.textWidth}, 600, 'easeInOutQuad', _.bind(function() {
                this.$itemTexts.append(this.$('.stacker-item-text').first()).css({marginLeft: 0});
            }, this));

        },

        onSorterClicked: function(event) {
            event.preventDefault();
            if (!this.parentView.model.get('_isEnabled')) {
                return;
            }
            if (this.pickerView) {
                this.removePickerViews();
            } else {
                this.removePickerViews();
                var availableIndexes = this.getAvailableIndexes();
                this.showIndexPicker(availableIndexes);
            }

        },

        removePickerViews: function() {
            this.parentView.trigger('remove:pickerViews');
        },

        onRemovePickerViews: function() {
            if (this.pickerView) {
                this.pickerView.remove();
                this.pickerView = undefined;
            }
        },

        showIndexPicker: function(availableIndexes) {
            this.pickerView = new StackerItemSorterPickerView({
                parentView: this,
                data: availableIndexes
            });
            this.$('.stacker-item-sorter').append(this.pickerView.$el);
        },

        getAvailableIndexes: function() {
            var availableIndexes = [];
            for (var i = 0; i < this.itemsLength; i++) {
                if (i != this.index) {
                    availableIndexes.push(i);
                }
            };
            return availableIndexes;
        },

        getState: function() {
            return {
                stage: this.stage
            }
        },

        updatePosition: function(newPosition) {
            // Triggered by the sorterPicker view
            // Remove picker view so cloned element doesn't contain it
            this.onRemovePickerViews();
            // Get top position of element
            var currentTop = this.$el.position().top;
            var originalWidth = this.$el.width();
            // First clone this item to hold the block
            var $clondedItem = this.$el.clone().css({
                visibility: 'hidden'
            });

            // Make original item positioned over the top ready for animation
            this.$el.css({
                position: 'absolute',
                zIndex: 1,
                width: originalWidth,
                top: currentTop + 'px'
            })

            // Work out where to insert both the cloned item and current item
            if (newPosition < this.index) {
                this.parentView.$('.stacker-item').eq(newPosition).before($clondedItem);
                this.parentView.$('.stacker-item').eq(newPosition).before(this.$el);
            } else if (newPosition > this.index) {
                this.parentView.$('.stacker-item').eq(newPosition).after($clondedItem);
                this.parentView.$('.stacker-item').eq(newPosition).after(this.$el);
            }

            // Animate the item into place
            var newTop = $clondedItem.position().top;

            this.$el.velocity({
                top: newTop + 'px'
            }, {
                duration: 1000,
                easing: [0.8, 0, 0.2, 1],
                complete: _.bind(function() {
                    // Once done reset styles to show original and remove cloned item
                    this.$el.attr('style', '');
                    $clondedItem.remove();
                    // After everything is done let's update the indexes
                    this.parentView.updateIndexes(this.index, newPosition);
                }, this)
            });

            

            /*this.$el.after($clondedItem);*/
        },

        showCorrectText: function(data) {
            // When showing the correct text we need to find the _options that is _isCorrect
            // check against the current stage and then push it to that index if it's not equal
            var that = this;
            var correctIndex;
            _.each(data._options, function(option, index) {
                if (option._isCorrect) {
                    correctIndex = option._originalIndex;
                }
            });
            this.$('.stacker-item-text').each(function(optionIndex, element) {
                var dataIndex = parseInt($(element).attr('data-index'));
                if (dataIndex != correctIndex) {
                    that.$itemTexts.append($(element));
                }
            });            
        },

        showUserAnswerText: function(usersAnswer) {
            var that = this;
            // Move all items before this stage - keeping the infinity scroll mega!
            this.$('.stacker-item-text').each(function(optionIndex, element) {
                var dataIndex = parseInt($(element).attr('data-index'));
                if (usersAnswer._options[usersAnswer._stage]._originalIndex != dataIndex) {
                    that.$itemTexts.append($(element));
                }

            });
        },

        showMarking: function() {
            if (this.data.correct) {
                this.$el.addClass('correct').removeClass('incorrect');
            } else {
                this.$el.addClass('incorrect').removeClass('correct');
            }
        }

    }, {
        template: 'stackerItem'
    });

    return StackerItemView;

})