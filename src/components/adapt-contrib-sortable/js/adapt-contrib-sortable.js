/*
* adapt-contrib-sortable
* License - http://github.com/adaptlearning/adapt_framework/LICENSE
* Maintainers - Sean Callahan <callahancg@gmail.com>
*/

define(function(require) {
    var QuestionView = require('coreViews/questionView');
    var Adapt = require('coreJS/adapt');
    var uiSort = require('components/adapt-contrib-sortable/js/jquery-ui.min');
    var correctOrder = [];

	var sortable = QuestionView.extend({

        events: {
        	'click .buttons .submitBtn': 'buttonClick',
        	'click .buttons .resetBtn': 'reset'
        },

        buttonClick: function(event) {
            this.checkAnswers();
            this.isCorrect();
            this.setupFeedback();
            this.setScore();
            this.showFeedback();
        },

        reset: function(event) {
            this.restoreOrginalAnswerOrder();
        },

        preRender: function() {
            this.getCorrectAnswerOrder();
            this.setupRandomisation();
        },

        postRender: function() {
            this.sortable();
            this.setupQuestion();
            this.setupFeedback();

            var sortableFixedHeight = $('.sortable-widget').outerHeight(false);
            this.$('.sortable-widget').css('height', sortableFixedHeight);

             this.setReadyStatus();
        },

        restoreOrginalAnswerOrder: function() {
            var itemOrder = this.model.get("_items");
            this.$('.sortable-item-inner').each(function(index) {
                $(this).html(itemOrder[index].text);
            });
        },

        getCorrectAnswerOrder: function() {
            var itemOrder = this.model.get("_items");
            if (itemOrder && itemOrder.length > 0) {
              for (var i = 0, l = itemOrder.length; i < l; i++) {
                  if (itemOrder[i].text) {
                    correctOrder[i] = itemOrder[i].text;
                  }
              }
            }
        },

        setupQuestion: function() {
            this.model.set('_itemOrder', []);
            this.setupQuestionItemIndexes();
            this.setScore();
            this.setupFeedback();
        },

        getNumQuestions: function() {
            //return this.model.get("_items").length;
        },

       checkAnswers: function() {
            var numCorrect = 0;
            var numQs = this.model.get("_items").length;;
            this.$(".sortable-item-inner").each(function(index) {
                if ($.trim($(this).text()) == correctOrder[index]) numCorrect++;
            });
            return numCorrect;
        },

        isCorrect: function() {
            var numberOfCorrectAnswers = this.checkAnswers();
            var numberOfAnswers = this.model.get('_items').length;

            this.model.set('_numberOfCorrectAnswers', numberOfCorrectAnswers);

            if (numberOfCorrectAnswers > 0) {
                this.model.set('_isAtLeastOneCorrectSelection', true);
            }
            if (numberOfCorrectAnswers === numberOfAnswers) {
                this.model.set('_isCorrect', true);
                return true;
            } else {
                this.model.set('_isCorrect', false);
                return false;
            }
        },

        setScore: function() {
            var questionWeight = this.model.get("_questionWeight");

            if (this.model.get('_isCorrect')) {
                this.model.set('_score', questionWeight);
                return;
            }

            var numberOfCorrectAnswers = this.checkAnswers();
            var itemLength = this.model.get('_items').length;

            var score = questionWeight * numberOfCorrectAnswers / itemLength;

            this.model.set('_score', score);
        },

        isPartlyCorrect: function() {
            return this.model.get('_isAtLeastOneCorrectSelection');
        },

        setupQuestionItemIndexes: function() {
            var items = this.model.get("_items");
            if (items && items.length > 0) {
                for (var i = 0, l = items.length; i < l; i++) {
                    if (items[i]._index === undefined) items[i]._index = i;
                }
            }
        },

    	setupRandomisation: function() {
            if (this.model.get('_isRandom') && this.model.get('_isEnabled')) {
                this.model.set("_items", _.shuffle(this.model.get("_items")));
            }
        },

    	sortable: function() {
    		/* JQUERY UI SORTABLE FOR REARRANGING ANSWER ORDER */
    		this.$(".sortable").sortable({
    			revert: 100,
    			containment: ".sortableWrap",
    			forceHelperSize: true,
    			cursor: "move",
    			scroll: true,
    			start: function(event, ui) {

    			},
    			change: function(event, ui) {

    			},
    			stop: function(event, ui) {

    			}
    		});

    		this.$(".sortable").on( "sort", function( event, ui ) {
                $('.ui-sortable-helper').addClass('lighten');
                if ($('html').hasClass("dir-rtl")) {
                    $(".ui-sortable-helper").css({
                        "box-shadow" : "-3px 3px 5px rgba(0,0,0,.5)",
                        "margin" : "0",
                        "right" : "0",
                     });
                } else {
                    $(".ui-sortable-helper").css({
        				"box-shadow" : "-3px 3px 5px rgba(0,0,0,.5)",
        				"margin" : "0",
        				"left" : "0",
        			 });
                 }
    		});

    		this.$(".sortable").on("sortstop", function( event, ui ) {
    			$(".sortable-item").attr("style", "");
                $(".sortable-item").removeClass('lighten');
                $('.ui-sortable-helper').removeClass('lighten');
                if ($('html').hasClass("dir-rtl")) {
                    $(".sortable-item").css("margin", "6px 12px 6px 0px");
                } else {
                    $(".sortable-item").css("margin", "6px 0 6px 12px");
                }
    		});
    	}

    });

    Adapt.register("sortable", sortable);

	return sortable;

});
