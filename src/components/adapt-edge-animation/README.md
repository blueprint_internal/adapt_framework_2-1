adapt-edge-animation
===================

A simple component to enable edge animations.

Installation
------------

Clone folder into src > components, and remove the hidden .git folder.

Usage
-----
Once installed, look at example.json within this package.
