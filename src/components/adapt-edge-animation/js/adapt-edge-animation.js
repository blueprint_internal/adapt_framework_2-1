/*
 * adapt-edge-animation
 * License - http://github.com/adaptlearning/adapt_framework/LICENSE
 */
define(function(require) {
	var ComponentView = require('coreViews/componentView');
	var Adapt = require('coreJS/adapt');
	var edgeJS = require('components/adapt-edge-animation/js/edge.5.0.0.min');

	var Animation = ComponentView.extend({
		edgeAnimation: function(){
			//console.log('EdgeAnimation');
			var edgeData = this.model.get('_edge');
			var dom = [ {"dom": [ ]}, {"dom": [ ]} ];

			// Adjust size of image
			if( edgeData.options.maxW.slice(0,-2) >= this.$('.component-inner').width() ){
				edgeData.options.maxW = this.$('.component-inner').width() + 'px';
			}

            var edgeProject = 'assets/'+edgeData.project;
			console.log('edgeData.project ' + edgeData.project)
			var existingComp = AdobeEdge.getComposition(edgeData.id)

			if( $.inArray(edgeData.id, aBootcompsLoaded) < 0 ){

				AdobeEdge.loadComposition(edgeProject, edgeData.id, edgeData.options, dom);

				var comp = this;
				AdobeEdge.bootstrapCallback(function(compId) {
					var composition = AdobeEdge.getComposition(compId)
					//var stage = composition.getStage();
					//stage.stop(0);
                    comp.edgeStage = composition.getStage();
					comp.edgeStage.stop(0);


					comp.$('.component-inner').on('inview', _.bind(comp.inview, comp));
					comp.$('.edge-poster').remove();
				});
			} else {
				this.edgeStage = existingComp.getStage();
				this.edgeStage.stop(0); // resets animation to beginning
				this.model.set('compositionStage',this.edgeStage) ;
				this.$('.component-inner').on('inview', _.bind(this.inview,this));
				this.$('.component-inner .edge-container').append(this.edgeStage.ele);
				this.$('.edge-poster').remove();
            }
		},

		playEdgeAnimation: function() {
			//console.log('Play');
			if( this.model.get('_playAnimation') ) {
				var composition = AdobeEdge.getComposition(this.model.get('_edge')['id']); // EDGE-xxxxxxx
				var stage = composition.getStage();
				stage.play();
				this.model.set('_playAnimation', false);
			}
		},

		preRender: function() {
			this.model.set('_playAnimation', true);
		},

		postRender: function() {
			this.edgeAnimation();
			this.setReadyStatus();
		},

		inview: function(event, visible, visiblePartX, visiblePartY) {
			//console.log('inview');
			if (visible) {
				if (visiblePartY === 'top') {
					this._isVisibleTop = true;
				} else if (visiblePartY === 'bottom') {
					this._isVisibleBottom = true;
				} else {
					this._isVisibleTop = true;
					this._isVisibleBottom = true;
				}

				if (this._isVisibleTop && this._isVisibleBottom) {
					this.$(this.model.get('cssSelector')).off('inview');
					this.setCompletionStatus();
					this.playEdgeAnimation();
				}
			}
		}

	});

	Adapt.register("edge-animation", Animation);
});
