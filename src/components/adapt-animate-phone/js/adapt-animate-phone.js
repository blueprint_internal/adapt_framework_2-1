/*
* adapt-animate-phone
* License - http://github.com/adaptlearning/adapt_framework/LICENSE
* Maintainers - Christian Murphy <cmurphy@unicon.net>
*/

define(function (require) {
    var ComponentView = require('coreViews/componentView');
    var Adapt = require('coreJS/adapt');
    var selector = '.animate-phone-widget';

    var animatePhone = ComponentView.extend({
        postRender: function () {
            this.$(selector).on('inview', _.bind(this.inview, this));
            this.setReadyStatus();
        },

        inview: function (event, visible) {
            if (visible) {
                $(selector).addClass('inview');
            } else {
                $(selector).removeClass('inview');
            }
        },

        remove: function () {
            // Remove any 'inview' listener attached.
            this.$(selector).off('inview', _.bind(this.inview, this));

            ComponentView.prototype.remove.apply(this, arguments);
        }
    });

    Adapt.register("animate-phone", animatePhone);
    return animatePhone;
});
