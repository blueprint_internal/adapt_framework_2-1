/*
* adapt-animate-box
* License - http://github.com/adaptlearning/adapt_framework/LICENSE
* Maintainers - Ryan Mathis <rmathis@unicon.net>
*/

define(function (require) {

    var ComponentView = require('coreViews/componentView');
    var Adapt = require('coreJS/adapt');
    var selector = '.animate-box-widget';

    var animateBox = ComponentView.extend({

        events: {

        },

        preRender: function () {
            this.selector = selector;
            this.listenTo(Adapt, 'device:changed', this.resizeImage);
            // Checks to see if the graphic should be reset on revisit
            this.checkIfResetOnRevisit();
        },

        postRender: function () {
            this.resizeComponentDiv();
            this.$(selector).on('inview', _.bind(this.inview, this));
            this.setReadyStatus();
        },

        resizeComponentDiv: function() {
            function resize() {
                var imgHeight = this.$('.animate-box-bgImg').outerHeight(true);
                var divHeight = this.$('.animate-box-widget').height();
                this.$('.animate-box-widget').css('height', imgHeight + 'px');
            }
            setTimeout(function() {
                resize();
            },500);

            window.addEventListener('resize', function () {
                resize();
            });
        },

        // Used to check if the graphic should reset on revisit
        checkIfResetOnRevisit: function () {
            var isResetOnRevisit = this.model.get('_isResetOnRevisit');

            // If reset is enabled set defaults
            if (isResetOnRevisit) {
                this.model.reset(isResetOnRevisit);
            }
        },

        inview: function (event, visible, visiblePartX, visiblePartY) {
            var list = this.$('.animate-box-item-list');
            if (visible) {
                list.addClass('messages-inview');
                this.setCompletionStatus();
            }
        },

        remove: function () {
            // Remove any 'inview' listener attached.
            this.$(selector).off('inview', _.bind(this.inview, this));

            ComponentView.prototype.remove.apply(this, arguments);
        }

    });

    Adapt.register("animate-box", animateBox);
    return animateBox;

});
