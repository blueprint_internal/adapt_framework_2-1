
define(function(require) {

    var ComponentView = require('coreViews/componentView');
    var Adapt = require('coreJS/adapt');

    var MobileAnimation = ComponentView.extend({

        events: {
            "click .content-popup-icon-close":"closeContent",
            "click .mobile-animation-item": "domPopup"
        },

        postRender: function() {
            var resources;
            var cssSelector;

            resources = this.model.get('_resources');
            this.setReadyStatus();
            this.animationActive = false;

            cssSelector = this.$('.static-flowchart-inner');
            if (!cssSelector) {
                this.setCompletionStatus();
            } else {
                this.model.set('cssSelector', cssSelector);
                this.$(cssSelector).on('inview', _.bind(this.inview, this));
            }

            $('')
        },

        inview: function(event, visible, visiblePartX, visiblePartY) {
            if (visible) {
                if (visiblePartY === 'top') {
                    this._isVisibleTop = true;
                } else if (visiblePartY === 'bottom') {
                    this._isVisibleBottom = true;

                } else {
                    this._isVisibleTop = true;
                    this._isVisibleBottom = true;
                }
                if (this._isVisibleTop && this._isVisibleBottom) {
                    this.$(this.model.get('cssSelector')).off('inview');
                    this.setCompletionStatus();
                    this.animation.init();

                }
            }
        },

        domPopup: function(event) {
            Adapt.trigger('notify:popup', {
               title: '',
               body: $(event.currentTarget).data('popup')
            });
        },

        closeDomPopup: function(event) {
            if (event) event.preventDefault();
            // trigger popup closed to reset the tab index back to 0
            Adapt.trigger('popup:closed');
        }

    });

    Adapt.register("mobile-animation", MobileAnimation);

});
