/*
* adapt-contrib-text
* License - http://github.com/adaptlearning/adapt_framework/blob/master/LICENSE
* Maintainers - Daryl Hedley <darylhedley@hotmail.com>, Brian Quinn <brian@learningpool.com>
*/
define(function(require) {

    var ComponentView = require('coreViews/componentView');
    var Adapt = require('coreJS/adapt');
    var uiPack = require('core/js/libraries/velocity.ui');
    var AnimateImageStack = ComponentView.extend({
        preRender: function() {
            this.listenTo(Adapt, 'device:changed', this.resizeDiv);
        },
        postRender: function() {
            this.setReadyStatus();
            this.resizeDiv(Adapt.device.screenSize);
            //this.$('audio, video').mediaelementplayer('#vid');
            //this.$('.mejs-offscreen').hide();
            // Check if instruction or body is set, otherwise force completion
            this.items = this.model.get("_items");
            this.cueAnimationAr = new Array();
            //this.$('.component-body').css({"position":"relative"});
            //this.$('.component-body').css({"height":"auto"});
            for(var i=0;i<this.items.length;i++)
            {
                var subj = this.$el.find('#'+i);
                subj.css({"z-index":String(i)});
                subj.css({"position":"absolute"});
                subj.css({"opacity":Number(this.items[i].opacity)});
                subj.css({"top":Number(this.items[i].top)});
                subj.css({"left":Number(this.items[i].right)});//I know..looks weird
                subj.css({"max-width":"800"});
                subj.css({"max-height":"500"});
                //subj.css({"font-size": "1.5vw"});

                /*
                if(this.items[i].animation!=undefined)
                {
                    this.cueAnimationAr.push(this.$el.find('#'+i));
                }
                */
            }
            var cssSelector = this.$('.component-instruction').length > 0 ? '.component-instruction' 
                : (this.$('.component-body').length > 0 ? '.component-body' : null);

            if (!cssSelector) {
                this.setCompletionStatus();
            } else {
                this.model.set('cssSelector', cssSelector);
                this.$(cssSelector).on('inview', _.bind(this.inview, this));
            }
            this.$('.component-inner').on('inview', _.bind(this.inview, this));
            this.$('.component-body').css({"margin-top":"0 !important"});
        },
        animate: function(){
            for(var i=0;i<this.items.length;i++)
            //{
                
                if(this.items[i].animation!=undefined)
                {
                    var subj = this.$el.find('#'+i)
                    subj.velocity({ opacity: 1 }, {delay:this.items[i].delay,duration:this.items[i].time }); 
                }
                
               
            //}
            
            
        },
        resizeDiv: function(width,height) {
            
          this.$('.component-inner').css({"width":width});
          this.$('.component-inner').css({"height":height});
        },

        inview: function(event, visible, visiblePartX, visiblePartY) {

            if (visible) {
                if (visiblePartY === 'top') {
                    this._isVisibleTop = true;
                } else if (visiblePartY === 'bottom') {
                    this._isVisibleBottom = true;
                } else {
                    this._isVisibleTop = true;
                    this._isVisibleBottom = true;
                }

                if (this._isVisibleTop && this._isVisibleBottom) {
                    this.animate();
                /* 
                    var vid = this.$("video");
                    vid[0].pause();
                    vid[0].currentTime = 0;
                    vid[0].play();
                */               
                    this.$(this.model.get('cssSelector')).off('inview');
                    this.setCompletionStatus();
                }
            }
        }
        
    });
    
    Adapt.register("animate-image-stack", AnimateImageStack);
    
});