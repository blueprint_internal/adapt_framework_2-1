define(function(require) {

    var QuestionView = require('coreViews/questionView');
    var Adapt = require('coreJS/adapt');
    var originalDraggableHeights = [];
    var intRightHandler;
    var intLeftHandler;
    var intTopHandler;
    var intBottomHandler;

    var dragDrop = QuestionView.extend({

        events: {
            'click .buttons .submitBtn': 'submit',
            'click .buttons .resetBtn': 'reset'
        },

        submit: function(event) {
            this.checkAnswers();
        },

        reset: function(event) {

        },

        preRender: function() {
            // Checks to see if the blank should be reset on revisit
            this.checkIfResetOnRevisit();
        },

        postRender: function() {
            this.displayAnswers();
            this.setReadyStatus();
            this.$('.component-inner').on('inview', _.bind(this.inview, this));
        },

        // Used to check if the blank should reset on revisit
        checkIfResetOnRevisit: function() {
            var isResetOnRevisit = this.model.get('_isResetOnRevisit');

            // If reset is enabled set defaults
            if (isResetOnRevisit) {
                this.model.reset(isResetOnRevisit);
            }
        },

        inview: function(event, visible, visiblePartX, visiblePartY) {
            if (visible) {
                if (visiblePartY === 'top') {
                    this._isVisibleTop = true;
                } else if (visiblePartY === 'bottom') {
                    this._isVisibleBottom = true;
                } else {
                    this._isVisibleTop = true;
                    this._isVisibleBottom = true;
                }

                if (this._isVisibleTop && this._isVisibleBottom) {
                    this.$('.component-inner').off('inview');
                    this.setCompletionStatus();
                }

            }
        },

        setupQuestionItemIndexes: function() {
            var items = this.model.get("_items");
            if (items && items.length > 0) {
                for (var i = 0, l = items.length; i < l; i++) {
                    if (items[i]._index === undefined) items[i]._index = i;
                }
            }
        },

		setupRandomisation: function() {
            if (this.model.get('_isRandom') && this.model.get('_isEnabled')) {
                this.model.set("_items", _.shuffle(this.model.get("_items")));
            }
        },

        /* GENERATE & DISPLAY ANSWERS AND DROP ZONES HTML */
        displayAnswers: function() {
        	var allAnswers = "";
        	var dropZones = "";
        	var answerDirection = "";
            var numcorrectAnswers = 0;
            var answers = this.model.get("_items");

            // Test for lang dir
            if ($('html').hasClass("dir-rtl")) {
                answerDirection = "rtl";
            } else {
                answerDirection = "ltr";
            }

        	// Create html for answers pool
        	 for (var i = 0, l = answers.length; i < l; i++) {
        		// Before every third iteration create a row
        		if (i % 3 == 0) {
        			allAnswers += '<div id="answers" class="answerRow reDroppable">';
        		}
        		// Create html for draggable element
        		if (answerDirection == "rtl") {
        			allAnswers += '<div id="draggable" class="drag_' + i + '  rtlStyle"><div class="draggableText rtlDrag">' + answers[i].text + '</div></div>';
        		} else {
        			allAnswers += '<div id="draggable" class="drag_' + i + '  ltrStyle"><div class="draggableText ltrDrag">' + answers[i].text + '</div></div>';
        		}
        		// Before every sixth iteration close a row
        		if (i % 3 == 2) {
        			allAnswers += '</div>';
        		}
                var answerIsCorrect = answers[i].correctAnswer;
                if (answerIsCorrect == "true") {
                    numcorrectAnswers++;
                }
        	}
        	// Create html for answer droppables
            // Test for correct answers here
        	for (var i=0; i< numcorrectAnswers; i++) {
        		// Before every second iteration create a row
        		if (i % 2 == 0) {
        			dropZones += '<div class="dropRow">';
        		}
        		// Create html for droppable element
        		dropZones += '<div id="droppable"></div>';
        		// Before every fourth iteration close a row
        		if (i % 2 == 1) {
        			dropZones += '</div>';
        		}
        	}
        	// Check to make sure all divs were closed on last iteration
        	if (allAnswers.substr(-18) != '</div></div></div>') allAnswers += '</div>';
        	if (dropZones.substr(-18) != '</div></div></div>') dropZones += '</div>';

        	// Display html
        	this.$(".answers").html(allAnswers);
        	this.$(".droppableZone").html(dropZones);

        	// Create drag and drop functionality after html has been generated
        	this.initializeDragDrop();
            this.initializeRedropable();
        	this.adjustHeights();
            /*
        	if (firstDivQuery) {
        		queryDivHeights();
        		firstDivQuery = false;
        	}
            */
        },

        /* Adjusts the heights of each draggable div to the size of its
         * child div draggableText. Adjusts all dropRow heights to fit
         * the largest answer plus margin & adjusts droppables to
         * largest answer size for consistant look. Sizes smaller than
         * the min-height are overridden by the css
         */
        adjustHeights: function() {
        	var largestRowHeight = 0;
        	var dropRowMargin = 10;
        	// Test height for each draggable text element
        	this.$(".answerRow > #draggable> div").each(function(index) {
        		var textHeight = $(this).height();
        		originalDraggableHeights[index] = $(this).parent().height();
        		// Find the largest element's height
        		if (textHeight > largestRowHeight) {
        			largestRowHeight = textHeight;
        		}
        		// Apply draggableText height to the draggable div
        		$('.drag_' + index).height(largestRowHeight);
        	});
        	// Adjust the height of dropZone rows plus margin
        	this.$('.droppableZone > div').height(largestRowHeight + dropRowMargin);
        	// Adjust the height of each droppable to match largest element
        	this.$(".dropRow > #droppable").each(function(index) {
        		$(this).height(largestRowHeight);
                var element = $(this).attr('id');
                var elementHeight = $(this).attr('style')
        	});
        },

        /* RESTORE THE ORIGINAL HEIGHT OF THE DRAGGABLE DIV */
        restoreOriginalDraggableHeight: function(element) {
        	var elementClass = element.attr("class");
        	var pattern = /\d+/;
        	var match = pattern.exec(elementClass);
        	var heightIndex = parseInt(match[0]);
        	return originalDraggableHeights[heightIndex];
        },

        /* INITIALIZE THE DRAG AND DROP BEHAVIOR */
        initializeDragDrop: function() {
        	// The parent div of the draggable element
        	var draggableDiv;

        	// Vars for keeping track of position within the scrollable div
        	var dropZoneDiv = this.$('.droppableZone');
        	var offset = dropZoneDiv.offset();
        	var offsetWidth = offset.left + dropZoneDiv.width();
        	var offsetHeight = offset.top + dropZoneDiv.height();

        	// Scroll behavior
        	var distance = 70;
        	var timer = 100;
        	var step = 10;

        	// Create the droppable elements
        	this.$(".dropRow > div").each(function() {
        		$(this).droppable({
        			accept: "#draggable",
        			tolerance: "intersect",
        			over: function( event, ui ) {
        				$(this).css("background-color", "#D9E4FF");
        			 },
        			 out: function( event, ui ) {
        				 $(this).css("background-color", "#fff");
        			 },
        			drop: function(event, ui) {
        				// Hide helper clone when dropped
        				$copy.css({
        					"display" : "none"
        				});
        				// Hide dragging element so revert animation isn't visible
        				$('.ui-draggable-dragging').css({
        					"display" : "none"
        				});
        				// Align original dragging element in droppable div
        				var newHeight = $(this).height();
        				$(ui.draggable).css({
        						"position" : "absolute",
        						"top" : "0",
        						"left" : "0",
        						"width" : "100%",
        						"max-width" : "456px",
        						"height" : newHeight + 'px',
        				});
        				$(this).css("background-color", "#fff");
        				// Append draggable html to droppable div
        				$(ui.draggable).appendTo(this);
        				/* If the div was dragged from the answer row, append a droppable div
        				    when its removed so it has a place to land if its redropped back into answers
        				if (draggableDiv.attr("class") == "answerRow") {
        					$(draggableDiv).addClass("reDroppable");
        					// Initialize the re-droppable div behavior after html is created
        					this.initializeRedropable();
        				}
                        */
        			},
        		});
        	});

        	// Create the draggable elements
        	this.$( ".answerRow > div" ).each(function() {
        		$(this).draggable({
        			cursor: 'move',
        			cursorAt: { top: 5, left: 5 },
        			containment: $(".step2"),
        			scroll: true,
        			refreshPositions: true,
        			helper: function () {
        				// Create a copy of the original draggable element
        				$copy = $(this).clone();
        				// Add dragging styles
        				$copy.css({
        					"background" : "#4267b2",
        					"font-weight" : "bold",
        					"border-radius" : "10px",
        					"min-height" : "40px",
        					"color" : "white",
        					"box-shadow" : "0px 3px 5px rgba(0,0,0,.5)",
        					"max-width" : "308px",
        				});
        				return $copy;
        			},
        			appendTo: 'body', // Draggable will fall behind elements unless its appended to body (JQUERY bug)
        			revertDuration: 200,
        			drag : function(e){
        				// Hide the original element while reverting only helper clone should be visible
        				$(this).css({
        					"display" : "none"
        				});
        			},
        			revert: function( event, ui ) {
        				// Apply dragging styles to reverting helper clone
        				$copy.css({
        					"background" : "#4267b2",
        					"font-weight" : "bold",
        					"border-radius" : "10px",
        					"min-height" : "40px",
        					"color" : "white",
        					"box-shadow" : "0px 3px 5px rgba(0,0,0,.5)"
        				});
        				return true;
        			},
        			start: function( event, ui ) {
        				// Get the parent div of dragging element
        				draggableDiv = $(this).parent();
        			},
        			stop: function( event, ui ) {
        				// Show original element
        				$(this).css({
        					"display" : "inline-block"
        				});
        			},
        		});
        	});

        	// Make draggable answers droppable for hover class and alert msg
        	this.$( ".answerRow > div" ).each(function() {
        		$(this).droppable({
        			hoverClass: "ui-state-active",
        			drop: function( event, ui ) {
        				var jItem = $(ui.draggable)
        				// Test for an empty slot and show alert if empty and draggable parent div is answers
        				if ($(event.target).find(".draggableText").html() != "" && draggableDiv.attr("id") == "answers") {
        					//Item already in cell
        					alert("Item already placed there")
        					return
        				}
        			},
        		});
        	});

        },

        /* INITIALIZE THE REDROPPABLE DIVS APPENDED TO ANSWERS POOL */
        initializeRedropable: function() {
        	this.$(".reDroppable").each(function() {
        		$(this).droppable({
        			accept: "#draggable",
        			tolerance: "intersect",
        			drop: function(event, ui) {
        				// Hide helper div clone
        				$copy.css({
        					"display" : "none"
        				});
        				// Hide draggable div
        				$('.ui-draggable-dragging').css({
        					"display" : "none"
        				});

        				// Reapply original answers styles
        				$(ui.draggable).css({
        					"position" : "relative",
        					"display" : "inline-block",
        					"z-index" : "11",
        					"width" : "33.3%",
        					"max-width" : "308px",
        					"min-height" : "35px",
        					"border" : "none !important",
        					"color" : "#444444 !important",
        					"cursor" : "move",
        				});
        				// Append html back to answers div
        				$(ui.draggable).appendTo(this);
        			},
        		});
        	});
        }

    });

    Adapt.register('dragDrop', dragDrop);

    return dragDrop;

});
