/*
* adapt-graphic-animation
* License - http://github.com/adaptlearning/adapt_framework/LICENSE
* Maintainers - Sean Callahan <scallahan@fb.com>
*/
define(function(require) {

    var ComponentView = require('coreViews/componentView');
    var Adapt = require('coreJS/adapt');

    var graphicAnimation = ComponentView.extend({

        events: {

        },

        preRender: function() {

        },

        postRender: function() {
            this.setReadyStatus();
            this.startAnimation();
            this.setCompletionStatus();
        },

        scaleText: function() {
            // Scales text widths to image containers
            setTimeout(function(){
                this.$(".graphicAnimation-wrap").each(function(index) {
                    var thisWidth = $(this).find(".graphicAnimation-img").width() + 'px';
                    $(this).find('.graphicAnimation-text').css('max-width', thisWidth);
                });
            }, 1000);
        },

        startAnimation: function() {
            // Save the this var to use in inner function
            var that = this;
            // Track window scrolling
            $(window).scroll(function() {
                // The scroll top of the window
                var scrollTop = $(this).scrollTop();
                /* For each animation component check to see if it
                   is in view and needs to be animated */
                this.$(".graphicAnimation-component").each(function() {
                    var elemHeight = $(this).height();
                    var offSet = $(this).offset();
                    /* If the scroll top is greater than the
                      element offset minus the element height */
                    if (scrollTop >= (offSet.top - elemHeight)) {
                       //that.animateElements();
                    }
                });
            });
        },

        animateElements: function() {
            var delay = this.model.get("delay") || 1000;
            // Apply delay to each element inside animation wrap
            this.$('.graphicAnimation-wrap > *').each(function (index) {
                $(this).css({ 'animation-delay': (index * delay) + 'ms' });
            });
            this.$('.graphicAnimation-img-top').addClass("graphicAnimation-slideInLeft");
            this.$('.graphicAnimation-img-bottom').addClass("graphicAnimation-slideInLeft");
            this.$('.graphicAnimation-topText').addClass("graphicAnimation-slideInLeftHalf");
            this.$('.graphicAnimation-midText').addClass("graphicAnimation-slideInLeftHalf");
            this.$('.graphicAnimation-bottomText').addClass("graphicAnimation-slideInLeftHalf");
        },

        inview: function(event, visible, visiblePartX, visiblePartY) {
            if (visible && visiblePartY === 'both') {
                this.$(selector).addClass('is-visible');
            }

            if (this._isVisibleTop && this._isVisibleBottom) {
                this.animateElements();
                this.$('.component-widget').off('inview');
                this.setCompletionStatus();
            }
        },

    });

    Adapt.register("graphicAnimation", graphicAnimation);

	return graphicAnimation;

});
