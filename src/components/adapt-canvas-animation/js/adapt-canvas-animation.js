
define(function(require) {

    var ComponentView = require('coreViews/componentView');
    var Adapt = require('coreJS/adapt');
    var Phaser = require('components/adapt-canvas-animation/js/phaser_closed.js');



    var CanvasAnimation = ComponentView.extend({

        events: {
            "click .content-popup-icon-close":"closeContent"
        },

        postRender: function() {
            this.setReadyStatus();
            // Check if instruction or body is set, otherwise force completion
            var cssSelector = this.$('.component-instruction').length > 0 ? '.component-instruction'
                : (this.$('.component-body').length > 0 ? '.component-body' : null);

            if (!cssSelector) {
                this.setCompletionStatus();
            } else {
                this.model.set('cssSelector', cssSelector);
                this.$(cssSelector).on('inview', _.bind(this.inview, this));
            }
            //this.$('.component-body').css({"margin-top":"0 !important"});
            var jsLink = this.model.get("animation");
            this.className = this.model.get("animationName");
            this.animationResources = this.model.get("_resources");
            this.loadjscssfile(jsLink, "js");
            console.log("class name", this.className);
            debugger;
            this.animation = new window[this.className](this.animationResources);
            _.extend(this.animation, Backbone.Events);
            this.animation.Adapt = Adapt;
            this.animation.on("domPopup",this.domPopup);


            //this.listenTo(this.animation, 'alert', this. domPopup);

        },


        inview: function(event, visible, visiblePartX, visiblePartY) {
            if (visible) {
                if (visiblePartY === 'top') {
                    this._isVisibleTop = true;
                } else if (visiblePartY === 'bottom') {
                    this._isVisibleBottom = true;

                } else {
                    this._isVisibleTop = true;
                    this._isVisibleBottom = true;


                }

                if (this._isVisibleTop && this._isVisibleBottom) {

                    this.$(this.model.get('cssSelector')).off('inview');
                    this.setCompletionStatus();
                    this.animation.init();

                }
            }
            else
            {
                /*
                if(this.animation!=undefined && this.animation.game.world)
                {
                    this.animation.game.world.removeAll();
                }
                */

            }
        },

        domPopup: function(evt) {


            var popupObject = new Object();

            Adapt.trigger('notify:popup', {
               title: evt.title,
               body: evt.body
            });

        },

        loadjscssfile: function(filename, filetype){

            var fileref=document.createElement('script');
            fileref.setAttribute("type","text/javascript");
            fileref.setAttribute("src", filename);
            this.$('.component-body').append(fileref);

        },

        closeDomPopup: function(event) {
            if (event) event.preventDefault();
            // trigger popup closed to reset the tab index back to 0
            Adapt.trigger('popup:closed');


        }



    });

    Adapt.register("canvas-animation", CanvasAnimation);

});
