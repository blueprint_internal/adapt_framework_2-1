/*
* adapt-twoColumn
* License - http://github.com/adaptlearning/adapt_framework/LICENSE
* Maintainers - Sean Callahan <callahancg@gmail.com>
*/

define(function(require) {

    var ComponentView = require('coreViews/componentView');
    var Adapt = require('coreJS/adapt');

	var twoColumn = ComponentView.extend({

		events: {

		},


        preRender: function() {

        },

        postRender: function() {
            this.setReadyStatus();
            this.$('.component-inner').on('inview', _.bind(this.inview, this));
            this.swapColDir();
        },

        // Used to check if the blank should reset on revisit
        checkIfResetOnRevisit: function() {
            var isResetOnRevisit = this.model.get('_isResetOnRevisit');

            // If reset is enabled set defaults
            if (isResetOnRevisit) {
                this.model.reset(isResetOnRevisit);
            }
        },

        inview: function(event, visible, visiblePartX, visiblePartY) {
            if (visible) {
                if (visiblePartY === 'top') {
                    this._isVisibleTop = true;
                } else if (visiblePartY === 'bottom') {
                    this._isVisibleBottom = true;
                } else {
                    this._isVisibleTop = true;
                    this._isVisibleBottom = true;
                }

                if (this._isVisibleTop && this._isVisibleBottom) {
                    this.$('.component-inner').off('inview');
                    this.setCompletionStatus();
                }

            }
        },

        swapColDir: function() {
            var imgDir = this.model.get('imageLayout');
            if (imgDir == "right") {
                this.$('.twoCol-leftCol').css('float', 'right');
                this.$('.twoCol-rightCol').css('float', 'left');
            }
        }

    });

    Adapt.register("twoColumn", twoColumn);
	return twoColumn;

});
