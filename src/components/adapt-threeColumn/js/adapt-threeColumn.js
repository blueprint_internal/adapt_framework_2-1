/*
* adapt-threeColumn
* License - http://github.com/adaptlearning/adapt_framework/LICENSE
* Maintainers - Sean Callahan <callahancg@gmail.com>
*/

define(function(require) {

    var ComponentView = require('coreViews/componentView');
    var Adapt = require('coreJS/adapt');

	var threeColumn = ComponentView.extend({

		events: {

		},


        preRender: function() {

        },

        postRender: function() {
            this.setReadyStatus();
            this.normalizeImgDivHeights();
            this.$('.component-inner').on('inview', _.bind(this.inview, this));
        },

        // Used to check if the blank should reset on revisit
        checkIfResetOnRevisit: function() {
            var isResetOnRevisit = this.model.get('_isResetOnRevisit');

            // If reset is enabled set defaults
            if (isResetOnRevisit) {
                this.model.reset(isResetOnRevisit);
            }
        },

        normalizeImgDivHeights: function() {

            function setHeight() {
                var largestHeight = 0;
                var height = 0;
                this.$(".threeCol-imgWrap").each(function(index) {
                    var height = $(this).height();
                    if (height > largestHeight) largestHeight = height;
                });

                if (largestHeight > 0) {
                    this.$(".threeCol-imgWrap").css('min-height', largestHeight + 'px');
                } else {
                    this.$(".threeCol-imgWrap").css('display', 'none');
                }
            }

            setTimeout(function() {
                setHeight();
            },1000);

        },

        inview: function(event, visible, visiblePartX, visiblePartY) {
            if (visible) {
                if (visiblePartY === 'top') {
                    this._isVisibleTop = true;
                } else if (visiblePartY === 'bottom') {
                    this._isVisibleBottom = true;
                } else {
                    this._isVisibleTop = true;
                    this._isVisibleBottom = true;
                }

                if (this._isVisibleTop && this._isVisibleBottom) {
                    this.$('.component-inner').off('inview');
                    this.setCompletionStatus();
                }

            }
        }

    });

    Adapt.register("threeColumn", threeColumn);
	return threeColumn;

});
