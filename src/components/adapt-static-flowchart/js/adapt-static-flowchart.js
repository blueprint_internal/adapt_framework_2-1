/*
* adapt-static-flowchart
*/
define(function(require) {

    var ComponentView = require('coreViews/componentView');
    var Adapt = require('coreJS/adapt');
    var uiPack = require('core/js/libraries/velocity.ui');
    var StaticFlowchart = ComponentView.extend({
        postRender: function() {
            var items;
            var cssSelector;

            items = this.model.get('_items');
            //console.log(items);
            this.setReadyStatus();
            this.animationActive = false;

            cssSelector = this.$('.static-flowchart-inner');
            if (!cssSelector) {
                this.setCompletionStatus();
            } else {
                this.model.set('cssSelector', cssSelector);
                this.$(cssSelector).on('inview', _.bind(this.inview, this));
            }
        }
    });

    Adapt.register('static-flowchart', StaticFlowchart);
    return StaticFlowchart;
});
