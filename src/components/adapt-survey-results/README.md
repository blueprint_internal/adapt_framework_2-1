#adapt-survey-results

This component compiles a range of results for display to the user.


##Installation

Clone the repo into your project:
        [project_name]\src\components

##Survey Components

Although these components look and function similar to existing components (textInput, mcq or gmcq) they inherit directly from the component class; this is different than standard question components which inherit from the question class and ties into the tutor system. Also unique to these survey components is the ability to detect content changes and store the new value automatically, and the ability to maintain state when navigating to different pages, articles, and components.

##Attributes

####resultsBody (string) (HTML enabled)

If any conditionals are satisfied, A dynamic section of content is shown before the results section.

####resultsPostBody (string) (HTML enabled)

If any conditionals are satisfied, A dynamic section of content is shown after the results section.

####resultsFor (enum)

Allowable types are: "course" (default), "page", or "component".

####componentIds (array of strings) (Enabled when resultsFor == "component")

A list of survey components to provide a response.

####showQuestion (boolean)

True will present a copy of the survey's body before displaying the response.

####prefix (string) (HTML enabled)

Part of the results section, this area precedes each response.

####suffix (string) (HTML enabled)

Part of the results section, this area follows each response.

##Example

See the example.json for structuring.

##Known Problems

None at this time.

