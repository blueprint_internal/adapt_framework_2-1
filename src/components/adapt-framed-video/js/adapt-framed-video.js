/*
* adapt-contrib-text
* License - http://github.com/adaptlearning/adapt_framework/blob/master/LICENSE
* Maintainers - Daryl Hedley <darylhedley@hotmail.com>, Brian Quinn <brian@learningpool.com>
*/
define(function(require) {

    var ComponentView = require('coreViews/componentView');
    var Adapt = require('coreJS/adapt');
    var mep = require('components/adapt-contrib-media/js/mediaelement-and-player');
    var FramedVideo = ComponentView.extend({
        preRender: function() {
           this.listenTo(Adapt, 'device:resize', this.onScreenSizeChanged);
            this.listenTo(Adapt, 'device:changed', this.onDeviceChanged);
        },

        postRender: function() {
            this.setReadyStatus();

            //this.$('audio, video').mediaelementplayer('#vid');
            //this.$('.mejs-offscreen').hide();
            // Check if instruction or body is set, otherwise force completion
            var cssSelector = this.$('.component-instruction').length > 0 ? '.component-instruction'
                : (this.$('.component-body').length > 0 ? '.component-body' : null);

            if (!cssSelector) {
                this.setCompletionStatus();
            } else {
                this.model.set('cssSelector', cssSelector);
                this.$(cssSelector).on('inview', _.bind(this.inview, this));
            }
            this.$('.component-inner').on('inview', _.bind(this.inview, this));

            var videoStyle = this.$(".framed-video-inner");
            if(this.model.get("_videoPosition") == "top")
            {
                videoStyle.css('z-index', '5');
            }
            //videoStyle.css('padding-top', this.model.get("_videoPt").y);
            //videoStyle.css('padding-left', this.model.get("_videoPt").x);

            if(this.model.get("_componentHeight")!=undefined)
            {
                this.$el.css('height',this.model.get("_componentHeight"));
            }
            if(this.model.get("_componentWidth")!=undefined)
            {
                this.$el.css('width',this.model.get("_componentWidth"));
            }

        },
        onDeviceChanged: function() {
           if(this.model.get("_componentWidth")!=undefined)
            {
                this.$el.css('width',this.model.get("_componentWidth"));
            }
        },
        onScreenSizeChanged: function() {

        },

        inview: function(event, visible, visiblePartX, visiblePartY) {
            var vid = this.$("video");

            // Video is inview
            if (visible && visiblePartY === 'both') {

                if(vid[0] != undefined) {

                    var poster = this.model.get('poster');
                    if (poster) {
                        vid[0].poster = poster;
                    }

                    // Set html5 video autoplay attribute
                    var autoPlay = this.model.get('autoPlay');
                    if (autoPlay == 'true') {
                        vid[0].play();
                        vid[0].autoPlay = true;
                        console.log('play video')
                    } else {
                        vid[0].autoPlay = false;
                    }

                    // Set html5 video controls attribute
                    var isControls = this.model.get('isControls');
                    if (!isControls) {
                        vid[0].controls = false;
                    } else if (isControls == 'true') {
                        vid[0].controls = true;
                    }

                    // Set html5 video mute attribute
                    var mute = this.model.get('_mute');
                    if(mute){
                       mute?vid[0].muted = true:vid[0].muted = false;
                    }

                    // Set html5 video loop attribute
                    var loop = this.model.get('loop');
                    if (loop == 'true') {
                        vid[0].loop = true;
                    } else {
                        vid[0].loop = false;
                    }

                    this.$(this.model.get('cssSelector')).off('inview');
                }
                this.setCompletionStatus();
            // Video is not inview
            } else {
                if(vid[0] != undefined){
                    vid[0].pause();
                }
            }
        }

    });

    Adapt.register("framed-video", FramedVideo);

});
