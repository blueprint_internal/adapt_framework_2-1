#adapt-survey-feedback

This component compiles a range of results and then runs through a list of conditionals to alter responses before displaying to the user.


##Installation

Clone the repo into your project:
        [project_name]\src\components

##Survey Components

Although these components look and function similar to existing components (textInput, mcq or gmcq) they inherit directly from the component class; this is different than standard question components which inherit from the question class and ties into the tutor system. Also unique to these survey components is the ability to detect content changes and store the new value automatically, and the ability to maintain state when navigating to different pages, articles, and components.

##Attributes

####feedbackBody (string) (HTML enabled)

If any conditionals are satisfied, A dynamic section of content is shown before the results section.

####feedbackPostBody (string) (HTML enabled)

If any conditionals are satisfied, A dynamic section of content is shown after the results section.

####prefix (string) (HTML enabled)

Part of the results section, this area precedes each response.

####suffix (string) (HTML enabled)

Part of the results section, this area follows each response.

####limitFeedback (boolean)

True will process top-down and return the first satisfied conditional; false will evaluate and return all satisfied conditionals.

####feedback (array of objects)

An ordered listing of conditionals to process.

####resultsFor (array of strings)

The survey results to be provided in each conditional; this is a list of component ids.

####condition (enum)

Allowable comparison types are: "is", "is not", or "contains"; empty or incorrect types are skipped.

####value (string)

The requirement a survey result must satisfy.

####response (string)

Content to return if the conditional is satisfied.

##Example

See the example.json for structuring.

##Known Problems

None at this time.

