define(function(require) {

    var ComponentView = require('coreViews/componentView');
    var Adapt = require('coreJS/adapt');

    var RemoteText = ComponentView.extend({

        preRender: function() {
            // Checks to see if the text should be reset on revisit
            this.checkIfResetOnRevisit();
            
            this.model.on('remoteSubmission',this.updateTextBody,this);   
        },

        updateTextBody: function(evt){

            //this.remotetextID = this.model.get("remotetextID")

        //$(this.model.get("remotetextID")).html(this.model.get("canvasText"));

         this.$('.component-body').html(this.model.get("canvasText"));




        //console.log("this.model.canvasText"+this.model.attributes.canvasText);

        ///WORKING SOLUTIONS

        //This replaces the text as expected
        //$("#test1").html("<b>WOOT Hello world!</b>");

        //This replaces the content of the specified body tage but does not update the page
        //Adapt.findById('c-03').attributes.body = "<p>Exactly</p>"

        //This replaces all body text
        //    $('.component-body-inner').text(" A Hello world!");
        
        //This replaces all HTML in body text with new html
        //$('.component-body-inner').html("<b> Be bold </b>");

        //$(cBlen5).text("q Hello world!");

        //////////////////////////////////////////


        
        },

        postRender: function() {
            this.setReadyStatus();

            // Check if instruction or body is set, otherwise force completion
           
            var cssSelector = this.$('.component-instruction').length > 0
                ? '.component-instruction'
                : (this.$('.component-body').length > 0 ? '.component-body' : null);

            if (!cssSelector) {
                this.setCompletionStatus();
            } else {
                this.model.set('cssSelector', cssSelector);
                this.$(cssSelector).on('inview', _.bind(this.inview, this));
            }

            /////////////////////////

            this.$('.component-body').css({"margin-top":"0 !important"});
 

           // this.animationResources = this.model.get("_resources");

        },

        // Used to check if the text should reset on revisit
        checkIfResetOnRevisit: function() {
            var isResetOnRevisit = this.model.get('_isResetOnRevisit');
            // If reset is enabled set defaults
            if (isResetOnRevisit) {
                this.model.reset(isResetOnRevisit);
            }
        },

        inview: function(event, visible, visiblePartX, visiblePartY) {


            if (visible) {
                if (visiblePartY === 'top') {
                    this._isVisibleTop = true;
                } else if (visiblePartY === 'bottom') {
                    this._isVisibleBottom = true;
                } else {
                    this._isVisibleTop = true;
                    this._isVisibleBottom = true;
                }
                if (this._isVisibleTop && this._isVisibleBottom) {
                    this.$(this.model.get('cssSelector')).off('inview');
                    this.setCompletionStatus();
                }
            }
        }

    });

    Adapt.register('remotetext', RemoteText);

    return RemoteText;

});
