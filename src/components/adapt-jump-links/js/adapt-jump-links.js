/*
* adapt-survey-radio
* License - http://github.com/adaptlearning/adapt_framework/blob/master/LICENSE
*/
define(function(require) {
	var ComponentView = require('coreViews/componentView');
	var Adapt = require('coreJS/adapt');

	var jumpLinks = ComponentView.extend({

		events:function() {
			return {
				"change .jump-link-item-input": "saveValue"
			}
		},

		preRender: function() {

		},

		postRender: function() {
			this.setReadyStatus();
			this.setTextboxHeight();
			this.$('.component-inner').on('inview', _.bind(this.inview, this));
		},

		saveValue: function(e) {
			var element = e.target;
            this.model.set('userSelection', element.value);
			this.setSelection();

            //this.$('.component-body').css("zoom","100%")
            document.body.style.zoom=1;

            //document.body.style.z-index
            //window.innerWidth=screen.width
		},

		setSelection: function() {
			var selection = this.model.get('userSelection');
			var element = this.el.getElementsByTagName('input');
			for(var i = 0; i < element.length; i++) {
				$(element[i].parentNode).removeClass("selected");
				if(element[i].value === selection) {
					element[i].checked = true;
					$(element[i].parentNode).addClass("selected");
				}
			}

			var item = this.model.get("_items");
			var navLink;
			this.$('.jump-link-item> label').each(function(index) {
				if ($(this).hasClass('selected')) {
					var linkText = item[index].text;
					navLink = $(this).parent().attr('data-to');
					$('.jump-link-text').html(linkText);
					$('.jump-link-text').addClass('jumpLinkFadeIn');
				}
			});

			this.navigateTo(navLink);
		},

		navigateTo: function(navLink) {
		   setTimeout(function() {
			   Backbone.history.navigate('#/id/' + navLink, true);
		   },3000);
	   },

		setTextboxHeight: function() {
			function resizeBox() {
				var height = 0;
				var paddingTop = parseInt($('.jump-link-text-box').css('padding-top'));
				var paddingBottom = parseInt($('.jump-link-text-box').css('padding-bottom'));
				var totalPadding = paddingTop + paddingBottom;

				this.$('.jump-link-item').each(function(index) {
					height += $(this).outerHeight(true);
				});
				height = height - totalPadding;
				$('.jump-link-text-box').css('height', height + 'px');
			}

			setTimeout(function() {
	            resizeBox();
	        },1000);
		},

		inview: function(event, visible, visiblePartX, visiblePartY) {
            if (visible) {
                if (visiblePartY === 'top') {
                    this._isVisibleTop = true;
                } else if (visiblePartY === 'bottom') {
                    this._isVisibleBottom = true;
                } else {
                    this._isVisibleTop = true;
                    this._isVisibleBottom = true;
                }

                if (this._isVisibleTop && this._isVisibleBottom) {
                    this.$('.component-inner').off('inview');
                    this.setCompletionStatus();
                }

            }
        }
	});

	Adapt.register("jumpLinks", jumpLinks);
	return jumpLinks;
});
