#adapt-survey-radio

This component provides a list of selectable items where only one item can be selected at any time; selecting a different item will un-selects the previous selection. The component has a hidden feature when provided a collection to "_dynamicBlocks" will auto hide/show any listed blocks throughout the current adapt course.


##Installation

Clone the repo into your project:
        [project_name]\src\components

##Survey Components

Although these components look and function similar to existing components (textInput, mcq or gmcq) they inherit directly from the component class; this is different than standard question components which inherit from the question class and ties into the tutor system. Also unique to these survey components is the ability to detect content changes and store the new value automatically, and the ability to maintain state when navigating to different pages, articles, and components.

##Attributes

####_dynamicBlocks (object of objects)

This item contains a collection of key/value pairs in the format "key": ["value", etc...] where each key is the value from a single _items object and value is a list of block ids that will dynamically become hidden/shown based on user's selected.

####userSelection (array of strings)

The user's selected value; this can be set beforehand.

####_items (array of objects)

Provides a listing of all available options.

####label (string)

A label to accompany the value; if provided, this will be displayed instead of the actual value.

####value (string)

REQUIRED for an item to appear as a selectable object; this is the actual value that will be stored.

####_graphic (object)

This object provides a graphic with the selection; uses a standard img element.

####alt (string)

An img element's alt attribute.

####title (string)

An img element's title attribute.

####src (string)

An img element's src attribute.

##Example

See the example.json for structuring.

##Known Problems

None at this time.

