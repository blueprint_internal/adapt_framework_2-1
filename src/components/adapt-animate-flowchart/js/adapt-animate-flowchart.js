/*
* adapt-animate-flowchart
* License - http://github.com/adaptlearning/adapt_framework/blob/master/LICENSE
* Maintainers - Ryan Mathis <rmathis@unicon.net>
*/
define(function(require) {

    var ComponentView = require('coreViews/componentView');
    var Adapt = require('coreJS/adapt');
    var selector = '.animate-flowchart-component';
    var AnimateFlowChart = ComponentView.extend({
        events: {},
        postRender: function() {
            this.$(selector).on('inview', _.bind(this.inview, this));
            this.setFixedHeight();
            this.applyAnimations();
            this.setReadyStatus();
        },
        applyAnimations: function () {
            var list = this.$('.animate-flowchart-list'),
                animationOffset = this.model.get('_animationOffset') || 1000;
            list.find('.animate-flowchart-item').each(function (index) {
                $(this).css({ 'animation-delay': (index * animationOffset) + 'ms' });
            });
        },
        inview: function(event, visible, visiblePartX, visiblePartY) {
            if (visible && visiblePartY === 'both') {
                this.$(selector).addClass('is-visible');
            }

            if (this._isVisibleTop && this._isVisibleBottom) {
                this.$('.component-widget').off('inview');
                this.setCompletionStatus();
            }
        },

        setFixedHeight: function() {
            function getHeight() {
                var thisHeight = this.$('.animate-flowchart-list').outerHeight(true) + 'px';
                this.$('.animate-flowchart-list').css('height', thisHeight);
            }

            setTimeout(function() {
                getHeight();
            },500);
        }

    });

    Adapt.register("animate-flowchart", AnimateFlowChart);
    return AnimateFlowChart;
});
