/*
* adapt-animate-text-blocks
* License - http://github.com/adaptlearning/adapt_framework/blob/master/LICENSE
* Maintainers - Ryan Mathis <rmathis@unicon.net>
*/
define(function(require) {

    var ComponentView = require('coreViews/componentView');
    var Adapt = require('coreJS/adapt');
    var selector = '.animate-text-blocks-component';
    var AnimateMessage = ComponentView.extend({

        events: {
            "click .animate-text-blocks-item.popup-active": "openPopup"
        },

        postRender: function() {
            this.$(selector).on('inview', _.bind(this.inview, this));
            this.applyAnimations(this.model.get('_animationOnView'));
            this.applyCustomStyles();
            this.setReadyStatus();
        },

        applyCustomStyles: function() {
            var spacing = this.model.get('_spacing'),
                corners = this.model.get('_cornerRadius'),
                props = {},
                items = this.$('.animate-text-blocks-item');

            if (spacing) { props.marginBottom = spacing; }
            if (corners) { props.borderRadius = corners; }
            items.css(props);
        },

        applyAnimations: function(animations) {
            var list = this.$('.animate-text-blocks-component');
            animations = _.isArray(animations) ? animations : _.isString(animations) ? animations.split(',') : [];
            _.forEach(animations, function (name) { list.addClass(name); });
            if (_.contains(animations, 'stagger')) {
                list.find('.animate-text-blocks-content').each(function (index) {
                    $(this).css({ animationDelay: (index * 500) + 'ms' });
                });
            }
        },

        inview: function(event, visible, visiblePartX, visiblePartY) {
            if (visible && visiblePartY === 'both') {
                this.$(selector).addClass('is-visible');
            }
            if (visible) {
                if (visiblePartY === 'top') {
                    this._isVisibleTop = true;
                } else if (visiblePartY === 'bottom') {
                    this._isVisibleBottom = true;
                } else {
                    this._isVisibleTop = true;
                    this._isVisibleBottom = true;
                }

                if (this._isVisibleTop && this._isVisibleBottom) {
                    this.$('.component-inner').off('inview');
                    this.setCompletionStatus();
                }
            }
        },

        openPopup: function (event) {
            event.preventDefault();
            var currentItem = this.getItemByIndex(event.currentTarget.id);
            if (currentItem && currentItem.popup_body) {
                Adapt.trigger('notify:popup', {
                    title: currentItem.item_title,
                    body: currentItem.popup_body
                });
            }
        },

        getItemByIndex: function (index) {
            return this.model.get('_items')[index];
        }

    });

    Adapt.register("animate-text-blocks", AnimateMessage);
    return AnimateMessage;
});
