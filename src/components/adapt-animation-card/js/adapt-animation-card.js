/*
* adapt-contrib-text
* License - http://github.com/adaptlearning/adapt_framework/blob/master/LICENSE
* Maintainers - Daryl Hedley <darylhedley@hotmail.com>, Brian Quinn <brian@learningpool.com>
*/
define(function(require) {

    var ComponentView = require('coreViews/componentView');
    var Adapt = require('coreJS/adapt');
    var uiPack = require('core/js/libraries/velocity.ui');
    var AnimationCard = ComponentView.extend({
        
        postRender: function() {
            this.setReadyStatus();

            // Check if instruction or body is set, otherwise force completion
            /*
            var cssSelector = this.$('.component-instruction').length > 0 ? '.component-instruction' 
                : (this.$('.component-body').length > 0 ? '.component-body' : null);

            */
            var cssSelector = this.$('.animation-card-body');
            if (!cssSelector) {
                this.setCompletionStatus();
            } else {
                this.model.set('cssSelector', cssSelector);
                this.$(cssSelector).on('inview', _.bind(this.inview, this));
            }
            this.$('.component-body').css({"margin-top":"0 !important"});
            
            
            
            this.setupAnimation();
        },


        inview: function(event, visible, visiblePartX, visiblePartY) {
            if (visible) {
                
                if (visiblePartY === 'top') {
                    this._isVisibleTop = true;
                    
                    
                } else if (visiblePartY === 'bottom') {
                    this._isVisibleBottom = true;
                    
                } else {
                      
                    this._isVisibleTop = true;
                    this._isVisibleBottom = true;
                }

                if (this._isVisibleTop == true && this._isVisibleBottom == true) { 
                    this.animateTop();
                    this.$(this.model.get('cssSelector')).off('inview');
                    this.setCompletionStatus();
                }

            }
        },
        setupAnimation: function()
        {

            this.cnt = this.model.get("_items").length;
            for(var i=0;i<this.cnt;i++)
            {
                var subj = this.$el.find('#'+i);
                var subTitle = subj.find(subj.children()[0]);
                var subBody = subj.find(subj.children()[1]);
                subTitle.css({"opacity":"0"});
                //subBody.css({"opacity":"0"});
                var col = String(this.model.get('backgroundColor')).split(",")[i];
                //subTitle.css({'background-color':col});

            }
        },

        animateTop: function() {
            var cnt = this.model.get("_items").length;
            for(var i=0;i<cnt;i++)
            {

                var subj = this.$el.find('#'+i);
            
                var subBody = subj.find(subj.children()[0]);
                var titleAnimation = this.model.get("titleAnimation");
                var bodyAnimation = this.model.get("bodyAnimation");
                var delay = this.model.get("animationStartDelay");
                subBody.velocity(bodyAnimation,{delay:(delay*500)+(500*i)});
            
            }
        }
        
        
    });
    
    Adapt.register("animation-card", AnimationCard);
    
});