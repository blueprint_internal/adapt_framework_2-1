/*
* adapt-threeColumn
* License - http://github.com/adaptlearning/adapt_framework/LICENSE
* Maintainers - Sean Callahan <callahancg@gmail.com>
*/

define(function(require) {

    var ComponentView = require('coreViews/componentView');
    var Adapt = require('coreJS/adapt');

	var animateIcon = ComponentView.extend({

		events: {

		},


        preRender: function() {

        },

        postRender: function() {
            this.setReadyStatus();
            this.$('.component-inner').on('inview', _.bind(this.inview, this));
            this.resizeItems();
        },

        // Used to check if the blank should reset on revisit
        checkIfResetOnRevisit: function() {
            var isResetOnRevisit = this.model.get('_isResetOnRevisit');

            // If reset is enabled set defaults
            if (isResetOnRevisit) {
                this.model.reset(isResetOnRevisit);
            }
        },

        resizeItems: function() {
            var numIcons = $('.animateIcon-icon-wrap').length;
            var maxHeight = 0;
            var doubleMargin = 4;
            // Calculate height as a percent based on number of icons for each side
            if (numIcons > 2) {
                maxHeight = (100 / (numIcons / 2) - doubleMargin);
            } else {
                maxHeight = 100 - doubleMargin;
            }

            var iconHeight = 'height: ' + maxHeight + '%;';
            var leftWidth = 'width: ' + this.model.get('leftColWidth') + '; ';
            var rightWidth = 'width: ' + this.model.get('rightColWidth') + '; ';
            var leftStyle = leftWidth + iconHeight;
            var rightStyle = rightWidth + iconHeight;

            // Add stlyes to resize icons based on json values
            this.$('.animateIcon-icon-wrap').each(function(index) {
                if (index % 2 == 0) {
                    $(this).attr('style', leftStyle);
                } else {
                    $(this).attr('style', rightStyle);
                }
            });
        },

        animatePosition: function() {
            var animationOffset = this.model.get('_animationOffset') || 1000;
            var animationDelay = this.model.get('_animationDelay') || 1000;
            var numIcons = $('.animateIcon-icon-wrap').length;
            var topPos = 0;
            var leftWidth = 100 - parseInt(this.model.get('leftColWidth'));

            this.$('.animateIcon-icon-wrap').each(function(index) {
                if (index % 2 == 0) {
                    topPos = index * (100 / numIcons);
                    $(this).delay(animationDelay * index).animate({opacity: '1', top: + topPos + '%', left: '0%'}, animationOffset);
                } else {
                    $(this).delay(animationDelay * index).animate({opacity: '1', top:  + topPos + '%', left: + leftWidth + '%'}, animationOffset);
                }
            });
        },

        inview: function(event, visible, visiblePartX, visiblePartY) {
            if (visible && visiblePartY === 'both') {
                this.$('.animateIcon-icon-wrap').addClass('is-visible');
            }
            if (visible) {
                if (visiblePartY === 'top') {
                    this._isVisibleTop = true;
                } else if (visiblePartY === 'bottom') {
                    this._isVisibleBottom = true;
                } else {
                    this._isVisibleTop = true;
                    this._isVisibleBottom = true;
                }

                if (this._isVisibleTop && this._isVisibleBottom) {
                    this.$('.component-inner').off('inview');
                    this.setCompletionStatus();
                    this.animatePosition();
                }
            }
        }
    });

    Adapt.register("animateIcon", animateIcon);
	return animateIcon;

});
