define(function(require) {

    var ComponentView = require('coreViews/componentView');
    var Adapt = require('coreJS/adapt');
    var uiPack = require('core/js/libraries/velocity.ui');
    var ArrowBox = ComponentView.extend({
        
        events: {
            "click .popup":"launchPopup"
            

        },

        preRender: function() {
            // Checks to see if the text should be reset on revisit
            this.checkIfResetOnRevisit();
        },

        postRender: function() {
            this.color = this.model.get('_color');
            this.$('.component-inner').css({'background-color':this.color});
            this.$('.component-body').css({'opacity':0});
           // this.$('.component-inner').arrow('#ff0000');
            this.setReadyStatus();

            // Check if instruction or body is set, otherwise force completion
            var cssSelector = this.$('.component-instruction').length > 0
                ? '.component-instruction'
                : (this.$('.component-body').length > 0 ? '.component-body' : null);

            if (!cssSelector) {
                this.setCompletionStatus();
            } else {
                this.model.set('cssSelector', cssSelector);
                this.$(cssSelector).on('inview', _.bind(this.inview, this));
            }
            this.$el.css({'color':this.color});
            //border-top-color
        },

        // Used to check if the text should reset on revisit
        checkIfResetOnRevisit: function() {
            var isResetOnRevisit = this.model.get('_isResetOnRevisit');

            // If reset is enabled set defaults
            if (isResetOnRevisit) {
                this.model.reset(isResetOnRevisit);
            }
        },
        launchPopup: function(evt)
        {
            if (evt) event.preventDefault();
            Adapt.trigger('notify:popup', {
               title: this.model.get('_popupTitle'),
               body: this.model.get('_popupBody')
            });
        },
        
        inview: function(event, visible, visiblePartX, visiblePartY) {
            if (visible) {
                if (visiblePartY === 'top') {
                    this._isVisibleTop = true;
                    
                } else if (visiblePartY === 'bottom') {
                    this._isVisibleBottom = true;
                } else {
                    this._isVisibleTop = true;
                    this._isVisibleBottom = true;
                }

                if (this._isVisibleTop && this._isVisibleBottom) {
                    var bod = this.$('.component-body');
                    var delay = this.model.get('_delay');
                    bod.velocity('transition.slideUpIn',{delay:delay});
                    this.$(this.model.get('cssSelector')).off('inview');
                    this.setCompletionStatus();
                    var scormWrapper = require('extensions/adapt-contrib-spoor/js/scorm/wrapper').getInstance();
                    if(this.model.get("_trackingEvent")!=undefined)
                    {
                        Adapt.trigger('tracking_event',{title:this.model.get("title"),event:this.model.get("_trackingEvent")});
                    }
                    
                }
            }
        }

    });

    Adapt.register('arrowbox', ArrowBox);

    return ArrowBox;

});

