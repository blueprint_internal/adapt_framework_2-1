/*
* adapt-survey-checkbox
* License - http://github.com/adaptlearning/adapt_framework/blob/master/LICENSE
*/
define(function(require) {
	var ComponentView = require('coreViews/componentView');
	var Adapt = require('coreJS/adapt');

	var Component = ComponentView.extend({
		events: function() {
			return {
				'click .survey-checkbox-item-input':'saveValue'
			}
		},

		saveValue: function(e) {
			var element = e.target;
			var selection = this.model.get('userSelection');
			if(typeof selection !== "object") selection = [];
			toggleArrayValue(selection, element.value);
			if(selection.length === 0) selection = "";
			this.model.set('userSelection', selection);
			this.setSelection();
			Adapt.trigger("survey:refresh", this);

			function toggleArrayValue(array, value) {
				// search for existing value and remove
				for(var i = 0; i < array.length; i++) {
					if(array[i] === value) {
						array.splice(i,1);
						return;
					}
				}
				// else push value to array
				array.push(value);
				return;
			}
		},

		setSelection: function() {
			var selection = this.model.get('userSelection');
			var element = this.el.getElementsByTagName('input');
			for(var i = 0; i < element.length; i++) {
				$(element[i].parentNode).removeClass("selected");
				for(var j = 0; j < selection.length; j++) {
					if(element[i].value === selection[j]) {
						element[i].checked = true;
						$(element[i].parentNode).addClass("selected");
					}
				}
			}
		},

		validateSelection: function() {
			var selection = this.model.get('userSelection');
			if(typeof selection === "string" && selection.length !== 0) {
				selection = [selection];
				this.model.set('userSelection', selection);
			}
		},

		preRender: function() {
			this.validateSelection();
		},

		postRender: function() {
			this.setReadyStatus();

			// Select items currently set on model for userSelection
			this.setSelection();

			// Check if instruction or body is set, otherwise force completion
			var cssSelector = this.$('.component-instruction').length > 0 ? '.component-instruction'
				: (this.$('.component-body').length > 0 ? '.component-body' : null);

			if (!cssSelector) {
				this.setCompletionStatus();
			} else {
				this.model.set('cssSelector', cssSelector);
				this.$(cssSelector).on('inview', _.bind(this.inview, this));
			}
		},

		inview: function(event, visible, visiblePartX, visiblePartY) {
			if (visible) {
				if (visiblePartY === 'top') {
					this._isVisibleTop = true;
				} else if (visiblePartY === 'bottom') {
					this._isVisibleBottom = true;
				} else {
					this._isVisibleTop = true;
					this._isVisibleBottom = true;
				}

				if (this._isVisibleTop && this._isVisibleBottom) {
					this.$(this.model.get('cssSelector')).off('inview');
					this.setCompletionStatus();
				}
			}
		}
	});
	Adapt.register("survey-checkbox", Component);
});