define(function(require) {

    var ComponentView = require('coreViews/componentView');
    var Adapt = require('coreJS/adapt');
    var ButtonsView = require('coreViews/buttonsView');
    var velocityN = require('core/js/libraries/velocity.ui');
    var itemsA;
    var hidddenTextArray = [];
    var abq = false;
    var subj;
    var hiddenTextAccessNum = 0;
    var $selectedElement;
    var FlexboxList = ComponentView.extend({

        preRender: function() {

            // Checks to see if the text should be reset on revisit
            this.checkIfResetOnRevisit();
        },

        postRender: function() {
            this.setReadyStatus();

            // Check if instruction or body is set, otherwise force completion
            var cssSelector = this.$('.component-instruction').length > 0
                ? '.component-instruction'
                : (this.$('.component-body').length > 0 ? '.component-body' : null);
            if (!cssSelector) {
                this.setCompletionStatus();
            } else {
                this.model.set('cssSelector', cssSelector);
                this.$(cssSelector).on('inview', _.bind(this.inview, this));
            }

        this.$('h1').css("fontSize", Number(this.model.get('header_fontsize')));
        this.$('h2').css("fontSize", Number(this.model.get('bodytitle_fontsize')));
        this.$('h3').css("fontSize", Number(this.model.get('body_fontsize')));


        this.$('h1').css("color", this.model.get('header_fontcolor'));
        this.$('h2').css("color", this.model.get('bodytitle_fontcolor'));
        this.$('h3').css("color", this.model.get('body_fontcolor'));


        this.$('.headerflex').css("color", this.model.get('body_color'));
         this.$('.headerflex').css("color", this.model.get('body_color'));
          this.$('.headerflex').css("background", this.model.get('body_color'));

        this.$('.main flexcol').css("background", this.model.get('body_color'));


        },
        

        // Used to check if the text should reset on revisit
        checkIfResetOnRevisit: function() {

            abq = false;

            var isResetOnRevisit = this.model.get('_isResetOnRevisit');

            // If reset is enabled set defaults
            if (isResetOnRevisit) {

                this.model.reset(isResetOnRevisit);
            }
        },

        inview: function(event, visible, visiblePartX, visiblePartY) {
            if (visible) {

                if (visiblePartY === 'top') {
                    this._isVisibleTop = true;
                    
                } else if (visiblePartY === 'bottom') {
                    this._isVisibleBottom = true;
                } else {
                    this._isVisibleTop = true;
                    this._isVisibleBottom = true;
                }

                if (this._isVisibleTop && this._isVisibleBottom) {
                    this.$(this.model.get('cssSelector')).off('inview');
                    this.setCompletionStatus();
                }
            }
        }
    });

    Adapt.register('flexbox-list', FlexboxList);

    return FlexboxList;

});
