/*
* adapt-contrib-blank
* License - http://github.com/adaptlearning/adapt_framework/LICENSE
* Maintainers - Kevin Corry <kevinc@learningpool.com>
*/
define(function(require) {
  var ComponentView = require('coreViews/componentView');
  var Adapt = require('coreJS/adapt');
  var CreateJs = require('components/adapt-facebook-animationBox/js/createjs-2014.12.12.min.js');
  var AnimationBox = ComponentView.extend({
    preRender: function()
    {
       this.listenTo(Adapt, 'device:resize', this.resizeAnimation);
       //this.listenTo(Adapt, 'navigation:backButton', this.kill);
      // Adapt.on('pageView:postRender', function(view) {this.kill});
    },
    postRender: function() {
      
      this.setReadyStatus();
      this.loadAssets(this.model.get("_animationCanvas"),this.model.get("_animationScript"));
     
            var cssSelector = this.$('.component-instruction').length > 0 ? '.component-instruction' 
                : (this.$('.component-body').length > 0 ? '.component-body' : null);

            if (!cssSelector) {
                this.setCompletionStatus();
            } else {
                this.model.set('cssSelector', cssSelector);
                this.$(cssSelector).on('inview', _.bind(this.inview, this));
            }
      
      this.resizeAnimation(Adapt.device.screenSize);
     
      this.$('.component-inner').on('inview', _.bind(this.inview, this));
      //Adapt.on()
    },

    inview: function(event, visible, visiblePartX, visiblePartY) {
        if (visible) {
            if (visiblePartY === 'top') {
                this._isVisibleTop = true;
            } else if (visiblePartY === 'bottom') {
                this._isVisibleBottom = true;
            } else {
                this._isVisibleTop = true;
                this._isVisibleBottom = true;
                
               //this.animation.animate();
              if(this.animation == undefined)
              {
                this.resources  = this.model.get('_resources');
                switch(this.resources.animation)
                {
                    case "test1":
                      this.animation = new TestRun(this.$('.component-inner'),this.model.get("_resources"));
                    break
                    case "test2":
                      this.animation = new TestRun2(this.$('.component-inner'),this.model.get("_resources"));
                    break
                    case "test3":
                    //@font-family
                      this.animation = new TestRun3(this.$('.component-inner'),this.model.get("_resources"));
                    break
                    case "ballCombine":
                    //@font-family
                      this.animation = new BallCombine(this.$('.component-inner'),this.model.get("_resources"));
                    break
                    case "extendReach":
                    //@font-family
                      this.animation = new ExtendReach(this.$('.component-inner'),this.model.get("_resources"));
                    break
                    case "extendReach_2":
                    //@font-family
                      this.animation = new ExtendReach_2(this.$('.component-inner'),this.model.get("_resources"));
                    break

                }
                
                var targetWidth = this.$('.component-inner').width();
                var targetHeight = this.$('.component-inner').height();
                this.animation.resize(targetWidth,targetHeight)


              }
              else
              {
                this.animation.resetAnimation();
              }
              
                
            }
            if (this._isVisibleTop && this._isVisibleBottom) {
              //this.$('.component-inner').off('inview');
              // this.animation.animationComplete();
                this.setCompletionStatus();
            }
            
        }
        else
        {
            if(this.animation!=null)
            {
              this.animation.animationComplete();
            }
        }
    },

    loadAssets: function (stageAsset,script)
    {
      //console.log("stage"+stageAsset)
      var stage = this.$('.component-stage');
      this.$('.component-inner').append(stageAsset);
      //this.scriptAdd(script);
    },

    scriptAdd: function(script)
    {
      var s = document.createElement("script");
      s.type = "text/javascript";
      s.src = script;
      this.$('.component-inner').append(s);
    },

    resizeAnimation: function(width) 
    {
      var targetWidth = this.$('.component-inner').width();
      var targetHeight = this.$('.component-inner').height();
     // console.log("RESIZE"+targetWidth)
      if(this.animation){this.animation.resize(targetWidth,473)};
    },

    kill: function()
    {
      this.animation.animationComplete();
    }

  });

  Adapt.register("facebook-animationBox", AnimationBox);

});
