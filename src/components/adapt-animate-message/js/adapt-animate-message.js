/*
* adapt-contrib-text
* License - http://github.com/adaptlearning/adapt_framework/blob/master/LICENSE
* Maintainers - Daryl Hedley <darylhedley@hotmail.com>, Brian Quinn <brian@learningpool.com>
*/
define(function(require) {

    var ComponentView = require('coreViews/componentView');
    var Adapt = require('coreJS/adapt');
    var uiPack = require('core/js/libraries/velocity.ui');
    var AnimateMessage = ComponentView.extend({
        events: {
            "click .animate-message-item":"showItemContent",
            "click .content-popup-icon-close":"closeContent"

        },
        postRender: function() {
            this.setReadyStatus();
            this.animationActive = false;
            // Check if instruction or body is set, otherwise force completion
            /*
            var cssSelector = this.$('.component-instruction').length > 0 ? '.component-instruction'
                : (this.$('.component-body').length > 0 ? '.component-body' : null);

            */
            var cssSelector = this.$('.animate-message-inner');
            if (!cssSelector) {
                this.setCompletionStatus();
            } else {
                this.model.set('cssSelector', cssSelector);
                this.$(cssSelector).on('inview', _.bind(this.inview, this));
            }
            this.$('.component-body').css({"margin-top":"0 !important"});
            if(this.model.get("_spacing")!=undefined)
            {
                var amt = this.model.get("_spacing");
                this.$('.animate-message-item').css({"padding":amt});

            }
           //


            this.setupAnimation();
        },
        showContentWithItemIndex: function(index) {
            this.$(".hotgrid-content-item").css({
                display:"none"
            });
            this.$(".hotgrid-content-item").eq(index).css({
                display:"block"
            });

            var $content = this.$(".hotgrid-content");
            $content.css({
                marginTop: -($content.height() / 2) + "px"
            }).velocity({
                opacity: 1,
                translateY: 0
            },{
                display: "block"
            });

            this.$(".hotgrid-shadow").velocity({
                opacity: 1
            },{
                display: "block"
            });
        },

        inview: function(event, visible, visiblePartX, visiblePartY) {
            if (visible) {
                if(this.model.get("_animationOnView") == "top" && this.animationActive == false)
                {
                    this.animateTop();
                    this.animationActive = true;
                };
                if (visiblePartY === 'top') {
                    this._isVisibleTop = true;


                } else if (visiblePartY === 'bottom') {
                    this._isVisibleBottom = true;

                } else {

                    this._isVisibleTop = true;
                    this._isVisibleBottom = true;
                }

                if (this._isVisibleTop && this._isVisibleBottom) {
                    if(this.model.get("_animationOnView") == undefined){this.animateTop()};
                    this.$(this.model.get('cssSelector')).off('inview');
                    this.setCompletionStatus();
                }

            }
            else
            {
                this.animationActive = false;
            }
        },
        setupAnimation: function()
        {
            this.delay = this.model.get("_delay");
            if(this.delay == undefined)
            {
                this.delay = 800;
            }
            this.cnt = this.model.get("_items").length;
            for(var i=0;i<this.cnt;i++)
            {
                var subj = this.$el.find('#'+i);
                var subTitle = subj.find(subj.children()[0]);
                var subBody = subj.find(subj.children()[1]);
                var cornerRadius = this.model.get('_cornerRadius')
                if(cornerRadius){
                    subj.css({"border-radius":cornerRadius});
                }
                if(this.model.get('_disableAnimation')!= true)
                {
                    subTitle.css({"opacity":"0"});
                   subBody.css({"opacity":"0"});
                }
                else
                {
                    subTitle.css({"opacity":"1"});
                    subBody.css({"opacity":"1"});
                }
                var col = String(this.model.get('backgroundColor')).split(",")[i];
                subj.css({'background-color':col});
                var currentItem = this.getCurrentItem(i);
                if(currentItem.popup_body)
                {
                    subj.css( 'cursor', 'pointer' );
                    subTitle.css( 'cursor', 'pointer' );
                    subBody.css( 'cursor', 'pointer' );
                }
            }
        },

        animateTop: function() {
            this.animationActive = true;
            var cnt = this.model.get("_items").length;
            for(var i=0;i<cnt;i++)
            {

                var subj = this.$el.find('#'+i);
                var subTitle = subj.find(subj.children()[0]);
                var subBody = subj.find(subj.children()[1]);

                var titleAnimation = this.model.get("titleAnimation");
                var bodyAnimation = this.model.get("bodyAnimation");

                if(this.model.get('_disableAnimation')!= undefined)
                {
                    subTitle.css({"opacity":"1"});
                    subBody.css({"opacity":"1"});
                }
                else
                {
                    subTitle.velocity(titleAnimation,{delay:this.delay*i});
                    subBody.velocity(bodyAnimation,{stagger:350,delay:(this.delay+300)*i});
                }


            }
            //this.model.off()
        },
        getCurrentItem: function(index) {
            return this.model.get('_items')[index];
        },

        getVisitedItems: function() {
            return _.filter(this.model.get('_items'), function(item) {
                return item;
            });
        },
        showItemContent: function(event) {

            if (event) event.preventDefault();
            // trigger popupManager - this sets all tabindex elements to -1
            //Adapt.trigger('popup:opened');
            //debugger

            var currentItem = this.getCurrentItem(event.currentTarget.id);
            if(currentItem.popup_body)
            {

                var promptObject = {
                    title: currentItem.item_title,
                    body: currentItem.popup_body

                }

                $(".content-popup-icon-close").focus();

                Adapt.trigger('notify:popup', promptObject);
            }
        },
        showContentWithItemIndex: function(index) {
            this.$(".hotgrid-content-item").css({
                display:"none"
            });
            this.$(".hotgrid-content-item").eq(index).css({
                display:"block"
            });

            var $content = this.$(".hotgrid-content");
            $content.css({
                marginTop: -($content.height() / 2) + "px"
            }).velocity({
                opacity: 1,
                translateY: 0
            },{
                display: "block"
            });

            this.$(".hotgrid-shadow").velocity({
                opacity: 1
            },{
                display: "block"
            });
        }

    });

    Adapt.register("animate-message", AnimateMessage);
    return AnimateMessage;
});
