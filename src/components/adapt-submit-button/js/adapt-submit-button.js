define(function(require) {

    var ComponentView = require('coreViews/componentView');
    var Adapt = require('coreJS/adapt');
    var ButtonsView = require('coreViews/buttonsView');
    var SubmitButton = ComponentView.extend({

        events: {
            "click .component-inner":"onSubmitClicked"
        },

        preRender: function() {
            this.$el.addClass("no-state");
            // Checks to see if the blank should be reset on revisit
            this.checkIfResetOnRevisit();
        },

        addButtonsView: function() {
            
            this.listenTo(this.$('.component-inner'), 'buttons:submit', this.onSubmitClicked);
            
            //this.listenTo(this.buttonsView, 'buttons:showCorrectAnswer', this.onShowCorrectAnswerClicked);
            //this.listenTo(this.buttonsView, 'buttons:hideCorrectAnswer', this.onHideCorrectAnswerClicked);
            //this.listenTo(this.buttonsView, 'buttons:showFeedback', this.showFeedback);
        },

        onSubmitClicked: function() {
            //look for below
            //Adapt.findById('c-05').attributes._isCorrect
            console.log("submit and show feedbacks!!");
            var data = this.model.get('_watchedComponents');
            var expectedResultsAr = new Array();
            var extractedResultsAr = new Array();
            var watchedIds = new Array();
            for(var i = 0; i<data.length; i++)
            {
                watchedIds.push(data[i].id);
                Adapt.findById(data[i].id).trigger('remoteSubmission');
                //Adapt.trigger(,);
            }
            for(var i = 0; i<data.length; i++)
            {
                expectedResultsAr.push(data[i].output);
                extractedResultsAr.push(Adapt.findById(data[i].id).attributes._isCorrect)
            }

            var compare = _.difference(extractedResultsAr,expectedResultsAr);
             
            
            if(compare.length>0)
            {
                message = this.model.get("_incorrectMessage");
            }
            else
            {
                message = this.model.get("_correctMessage");
            }
            
            if(extractedResultsAr.indexOf(undefined)!=-1)
            {
                var message = this.model.get("_incompleteMessage");
            }
            //test commit
            this.model.set({
                feedbackMessage: message
            });
            Adapt.trigger('questionView:showFeedback', this);
            

            
        },

        postRender: function() {
            this.setReadyStatus();
            this.addButtonsView();

            this.$('.component-inner').on('inview', _.bind(this.inview, this));
        },

        // Used to check if the blank should reset on revisit
        checkIfResetOnRevisit: function() {
            var isResetOnRevisit = this.model.get('_isResetOnRevisit');

            // If reset is enabled set defaults
            if (isResetOnRevisit) {
                this.model.reset(isResetOnRevisit);
            }
        },

        inview: function(event, visible, visiblePartX, visiblePartY) {
            if (visible) {
                if (visiblePartY === 'top') {
                    this._isVisibleTop = true;
                } else if (visiblePartY === 'bottom') {
                    this._isVisibleBottom = true;
                } else {
                    this._isVisibleTop = true;
                    this._isVisibleBottom = true;
                }

                if (this._isVisibleTop && this._isVisibleBottom) {
                    this.$('.component-inner').off('inview');
                    this.setCompletionStatus();
                }

            }
        }

    });

    Adapt.register('submit-button', SubmitButton);

    return SubmitButton;

});
