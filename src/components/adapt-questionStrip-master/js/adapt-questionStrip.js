define([ "coreJS/adapt", "coreViews/questionView" ], function(Adapt, QuestionView) {

    var QuestionStrip = QuestionView.extend({

        events: function() {
            return _.extend({}, QuestionView.prototype.events, {
                'click .qs-controls':'onNavigationClicked'
            });
        },

        setDeviceSize: function() {
            if (Adapt.device.screenSize === 'large') {
                this.$el.addClass('desktop').removeClass('mobile');
                this.model.set('_isDesktop', true);
            } else {
                this.$el.addClass('mobile').removeClass('desktop');
                this.model.set('_isDesktop', false)
            }
        },

        // Used by question to disable the question during submit and complete stages
        disableQuestion: function() {
            this.$('.qs-controls').addClass('disabled');
        },

        // Used by question to enable the question during interactions
        enableQuestion: function() {
            this.$('.qs-controls').removeClass('disabled');
        },

        // Used by the question to reset the question when revisiting the component
        resetQuestionOnRevisit: function() {
            this.resetQuestion();
        },

        setupQuestion: function() {
            this.listenTo(Adapt, 'device:resize', this.resizeControl, this);
            this.setDeviceSize();
            this.restoreUserAnswers();
        },

        restoreUserAnswers: function() {
            if (!this.model.get("_isSubmitted")) return;

            var userAnswer = this.model.get("_userAnswer");

            _.each(this.model.get("_items"), function(item, index) {
                item._stage = userAnswer[index];
            });

            this.setQuestionAsSubmitted();
            this.markQuestion();
            this.setScore();
            this.showMarking();
            this.setupFeedback();
        },

        storeUserAnswer: function() {

            var userAnswer = new Array(this.model.get('_items').length);

            _.each(this.model.get('_items'), function(item, index) {
                userAnswer[index] = item._stage;
            }, this);
            
            this.model.set('_userAnswer', userAnswer);

        },

        onQuestionRendered: function() {
            this.setupImages();
            this.setupNarrative();
        },

        canSubmit: function() {
            return true;
        },

        isCorrect: function() {

            var numberOfCorrectAnswers = 0;

            _.each(this.model.get('_items'), function(item, index) {

                if (item.hasOwnProperty('_stage') && item._subItems[item._stage]._isCorrect) {
                    numberOfCorrectAnswers ++;
                    item._isCorrect = true;
                    this.model.set('_numberOfCorrectAnswers', numberOfCorrectAnswers);
                    this.model.set('_isAtLeastOneCorrectSelection', true);
                } else {
                    item._isCorrect = false;
                }

            }, this);

            this.model.set('_numberOfCorrectAnswers', numberOfCorrectAnswers);

            if (numberOfCorrectAnswers === this.model.get('_items').length) {
                return true;
            } else {
                return false;
            }

        },

        setScore: function() {
            var questionWeight = this.model.get("_questionWeight");

            if (this.model.get('_isCorrect')) {
                this.model.set('_score', questionWeight);
                return;
            }
            
            var numberOfCorrectAnswers = this.model.get('_numberOfCorrectAnswers');
            var itemLength = this.model.get('_items').length;

            var score = questionWeight * numberOfCorrectAnswers / itemLength;

            this.model.set('_score', score);
        },

        showMarking: function() {

            _.each(this.model.get('_items'), function(item, i) {

                var $item = this.$('.component-item').eq(i);
                $item.removeClass('correct incorrect').addClass(item._isCorrect ? 'correct' : 'incorrect');

            }, this);

        },

        isPartlyCorrect: function() {
            return this.model.get('_isAtLeastOneCorrectSelection');
        },

        resetUserAnswer: function() {
            this.model.set({_userAnswer: []});
        },

        resetQuestion: function() {
            this.$(".component-item").removeClass("correct").removeClass("incorrect");
            this.model.set('_isAtLeastOneCorrectSelection', false);
            _.each(this.model.get("_items"), function(item, index) {
                this.setStage(index, item.hasOwnProperty('_initialItemIndex') ? item._initialItemIndex : 0);
            }, this);
        },

        showCorrectAnswer: function() {

            _.each(this.model.get('_items'), function(item, index) {

                _.each(item._subItems, function(option, optionIndex) {
                    if (option._isCorrect) {
                        this.setStage(index, optionIndex);
                    }
                }, this);

            }, this);

        },

        hideCorrectAnswer: function() {
            
            _.each(this.model.get('_items'), function(item, index) {
                this.setStage(index, this.model.get('_userAnswer')[index]);
            }, this);
        },

        setupImages: function() {
            var _items = this.model.get("_items");
            var _images = this.model.get("_images");

            var splitHeight = _items.length;
            var images = _images.length;
            var imagesSplit = 0;

            var thisHandle = this;

            var height = undefined;
            var width = undefined;
            
            var imagesToLoadCount = 0;
            var imagesLoadedCount = 0;

            _.each(_images, function(image) {
                var imageObj = new Image();
                $(imageObj).bind("load", function(event) {
                    imagesSplit++;

                    var imgHeight = imageObj.naturalHeight;
                    var imgWidth = imageObj.naturalWidth;

                    height = height || imageObj.naturalHeight;
                    width = width || imageObj.naturalWidth;

                    var finalHeight = height / splitHeight;

                    var offsetTop = 0;
                    var offsetLeft = 0;
                    var newHeight = height;

                    if (imgWidth != width) newHeight = (width/imgWidth) * imgHeight;

                    var canvas = document.createElement("canvas");
                    if (typeof G_vmlCanvasManager != 'undefined') G_vmlCanvasManager.initElement(canvas);
                    canvas.width = width; 
                    canvas.style.width = "100%"; 
                    canvas.height = finalHeight; 

                    var ctx = canvas.getContext("2d");

                    for (var s = 0; s < splitHeight; s++) {
                        ctx.drawImage(imageObj, 0, 0, imgWidth, imgHeight, 0 + offsetLeft, -(s * finalHeight) + offsetTop, width, newHeight);
                        var imageURL = canvas.toDataURL();

                        var img = document.createElement("img");
                        thisHandle.$('.item-'+s+'.qs-slide-container .i'+image._id).append(img);
                        imagesToLoadCount++;
                        $(img).bind("load", function() {
                            imagesLoadedCount++;

                            if (imagesToLoadCount == imagesLoadedCount) {
                                thisHandle.setReadyStatus();
                                $(window).resize();
                            }
                        });
                        img.src = imageURL;

                    }

                    if (imagesSplit == images) {
                        thisHandle.calculateWidths();
                    }

                });
                imageObj.src = image.src;
            });
        },

        setupNarrative: function() {
            this.setDeviceSize();
            
            var _items = this.model.get("_items");
            var thisHandle = this;
            _.each(_items, function(item, index) {
                item._itemCount = item._subItems.length;
                if (item.hasOwnProperty('_stage')) {
                    thisHandle.setStage(index, item._stage, true);
                } else {
                    thisHandle.setStage(index, item.hasOwnProperty('_initialItemIndex') ? item._initialItemIndex : 0);
                }
            });
            
            this.model.set('_active', true);

        },

        calculateWidths: function() {
            //calc widths for each item
            var _items = this.model.get("_items");
            _.each(_items, function(item, index) {
                var slideWidth = this.$('.qs-slide-container').width();
                var slideCount = item._itemCount;
                var marginRight = this.$('.qs-slider-graphic').css('margin-right');

                var extraMargin = marginRight === "" ? 0 : parseInt(marginRight);
                var fullSlideWidth = (slideWidth + extraMargin) * slideCount;
                var iconWidth = this.$('.qs-popup-open').outerWidth();

                this.$('.item-'+index+'.qs-slide-container .qs-slider-graphic').width(slideWidth)
                this.$('.qs-strapline-header').width(slideWidth);
                this.$('.qs-strapline-title').width(slideWidth);

                this.$('.item-'+index+'.qs-slide-container .qs-slider').width(fullSlideWidth);
                this.$('.qs-strapline-header-inner').width(fullSlideWidth);

                var stage = item._stage;//this.model.get('_stage');
                var margin = -(stage * slideWidth);

                this.$('.item-'+index+'.qs-slide-container .qs-slider').css('margin-left', margin);
                this.$('.qs-strapline-header-inner').css('margin-left', margin);

                item._finalItemLeft = fullSlideWidth - slideWidth;
            });

            _.each(this.$('.qs-slider-graphic'), function(item) {
                $(item).attr("height","").css("height","");
            });
        },

        resizeControl: function() {
            this.setDeviceSize();
            this.calculateWidths();
            var _items = this.model.get("_items");
            var thisHandle = this;
            _.each(_items, function(item, index) {
                thisHandle.evaluateNavigation(index);
            });
        },

        moveSliderToIndex: function(itemIndex, stage, animate) {
            var extraMargin = parseInt(this.$('.item-'+itemIndex+'.qs-slide-container .qs-slider-graphic').css('margin-right')),
                movementSize = this.$('.item-'+itemIndex+'.qs-slide-container').width()+extraMargin;

            if(animate) {
                this.$('.item-'+itemIndex+'.qs-slide-container .qs-slider').stop().animate({'margin-left': -(movementSize * stage)});
                this.$('.item-'+itemIndex+' .qs-strapline-header .qs-strapline-header-inner').stop(true, true).animate({'margin-left': -(movementSize * stage)});
            } else {
                this.$('.item-'+itemIndex+'.qs-slide-container .qs-slider').css({'margin-left': -(movementSize * stage)});
                this.$('.item-'+itemIndex+' .qs-strapline-header .qs-strapline-header-inner').css({'margin-left': -(movementSize * stage)});
            }
        },

        setStage: function(itemIndex, stage, initial) {
            var item = this.model.get('_items')[itemIndex];
            item._stage = stage;
            item.visited = true;

            this.$('.qs-progress').removeClass('selected').eq(stage).addClass('selected');
            this.$('.item-'+itemIndex+'.qs-slide-container .qs-slider-graphic').children('.controls').attr('tabindex', -1);
            this.$('.item-'+itemIndex+'.qs-slide-container .qs-slider-graphic').eq(stage).children('.controls').attr('tabindex', 0);

            this.evaluateNavigation(itemIndex);

            this.moveSliderToIndex(itemIndex, stage, !initial);
        },

        evaluateNavigation: function(itemIndex) {
            var item = this.model.get('_items')[itemIndex];
            var currentStage = item._stage;
            var itemCount = item._itemCount;
            if (currentStage == 0) {
                this.$('.item-'+itemIndex+'.qs-slide-container .qs-control-left').addClass('qs-hidden');

                if (itemCount > 1) {
                    this.$('.item-'+itemIndex+'.qs-slide-container .qs-control-right').removeClass('qs-hidden');
                }
            } else {
                this.$('.item-'+itemIndex+'.qs-slide-container .qs-control-left').removeClass('qs-hidden');

                if (currentStage == itemCount - 1) {
                    this.$('.item-'+itemIndex+'.qs-slide-container .qs-control-right').addClass('qs-hidden');
                } else {
                    this.$('.item-'+itemIndex+'.qs-slide-container .qs-control-right').removeClass('qs-hidden');
                }
            }

        },

        getVisitedItems: function() {
          return _.filter(this.model.get('_items'), function(item) {
                return item.visited;
          });
        },

        onNavigationClicked: function(event) {
            event.preventDefault();

            var $target = $(event.currentTarget);

            if ($target.hasClass('disabled')) return;

            if (!this.model.get('_active')) return;

            var selectedItemIndex = $target.parent('.component-item').index();
            var selectedItemObject = this.model.get('_items')[selectedItemIndex];

            var stage = selectedItemObject._stage,
                numberOfItems = selectedItemObject._itemCount;

            if ($target.hasClass('qs-control-right')) {
                stage++;
                if (stage == numberOfItems-1) {
                    $('.qs-control-left').focus();
                }
            } else if ($target.hasClass('qs-control-left')) {
                stage--;
                if (stage == 0) {
                    $('.qs-control-right').focus();
                }
            }
            stage = (stage + numberOfItems) % numberOfItems;
            this.setStage(selectedItemIndex, stage);
        }

    });

    Adapt.register("questionStrip", QuestionStrip);

    return QuestionStrip;

});
