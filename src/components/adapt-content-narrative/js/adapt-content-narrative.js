define(function(require) {

    var ComponentView = require("coreViews/componentView");
    var Adapt = require("coreJS/adapt");

    var Content_Narrative = ComponentView.extend({

        events: {
            'touchstart .content-narrative-slider':'onTouchNavigationStarted',
            'click .content-narrative-popup-open':'openPopup',
            'click .notify-popup-icon-close':'closePopup',
            'click .content-narrative-controls':'onNavigationClicked'
        },

        preRender: function () {
            this.listenTo(Adapt, 'device:changed', this.reRender, this);
            this.listenTo(Adapt, 'device:resize', this.resizeControl, this);
            this.setDeviceSize();
            console.log('preRender');
        },

        setDeviceSize: function() {
            if (Adapt.device.screenSize === 'large') {
                this.$el.addClass('desktop').removeClass('mobile');
                this.model.set('_isDesktop', true);
            } else {
                this.$el.addClass('mobile').removeClass('desktop');
                this.model.set('_isDesktop', false)
            }
        },

        postRender: function() {
            console.log('postRender');
            this.$('.content-narrative-slider').imageready(_.bind(function(){
                this.setReadyStatus();
                var setCompletionOn = this.model.get('_setCompletionOn');
                if(setCompletionOn === 'inview') {
                    var cssSelector = this.$('.component-widget').length > 0 ? '.component-widget'
                        : (this.$('.component-instruction').length > 0 ? '.component-instruction'
                        : (this.$('.component-body').length > 0 ? '.component-body' : null));

                    if (!cssSelector) {
                        this.setCompletionStatus();
                    } else {
                        this.model.set('cssSelector', cssSelector);
                        this.$(cssSelector).on('inview', _.bind(this.inview, this));
                    }
                }
            }, this));
            this.setupNarrative();
        },

        inview: function(event, visible, visiblePartX, visiblePartY) {
            if (visible) {
                if (visiblePartY === 'top') {
                    this._isVisibleTop = true;
                } else if (visiblePartY === 'bottom') {
                    this._isVisibleBottom = true;
                } else {
                    this._isVisibleTop = true;
                    this._isVisibleBottom = true;
                }

                if (this._isVisibleTop && this._isVisibleBottom) {
                    this.$(this.model.get('cssSelector')).off('inview');
                    this.setCompletionStatus();
                }
            }
        },

        setupNarrative: function() {
            _.bindAll(this, 'onTouchMove', 'onTouchEnd');
            this.setDeviceSize();
            this.model.set('_itemCount', this.model.get('_items').length);

            this.model.set('_active', true);

            if (this.model.get('_stage')) {
                this.setStage(this.model.get('_stage'));
            } else {
                this.setStage(0);
            }
            this.calculateWidths();
        },

        calculateWidths: function() {
            var slideWidth = this.$('.content-narrative-slide-container').width();
            var slideCount = this.model.get('_itemCount');
            var marginRight = this.$('.content-narrative-slider-graphic').css('margin-right');
            var extraMargin = marginRight === "" ? 0 : parseInt(marginRight);
            var fullSlideWidth = (slideWidth + extraMargin) * slideCount;
            var iconWidth = this.$('.content-narrative-popup-open').outerWidth();
            //var sliderHeight = this.$('.content-narrative-slider').outerHeight();

            this.$('.content-narrative-slider-graphic').width(slideWidth)
            this.$('.content-narrative-strapline-header').width(slideWidth);
            this.$('.content-narrative-strapline-title').width(slideWidth);
            this.$('.content-narrative-strapline-title-inner').width(slideWidth - iconWidth);
            //this.$('.content-narrative-content').css('height', sliderHeight);

            this.$('.content-narrative-slider').width(fullSlideWidth);
            this.$('.content-narrative-strapline-header-inner').width(fullSlideWidth);
            console.log('fullSlideWidth ' + fullSlideWidth);

            var stage = this.model.get('_stage');
            var margin = -(stage * slideWidth);

            this.$('.content-narrative-slider').css('margin-left', margin);
            this.$('.content-narrative-strapline-header-inner').css('margin-left', margin);

            this.model.set('_finalItemLeft', fullSlideWidth - slideWidth);
        },

        resizeControl: function() {
            this.setDeviceSize();
            this.calculateWidths();
            this.evaluateNavigation();
        },

        reRender: function() {
            if (this.model.get('_wasHotgraphic') && Adapt.device.screenSize == 'large') {
                this.replaceWithHotgraphic();
            }
        },

        replaceWithHotgraphic: function () {
            var Hotgraphic = require('components/adapt-contrib-hotgraphic/js/adapt-contrib-hotgraphic');
            var model = this.prepareHotgraphicModel();
            var newHotgraphic = new Hotgraphic({model:model, $parent: this.options.$parent});
            this.options.$parent.append(newHotgraphic.$el);
            this.remove();
            _.defer(function(){
                Adapt.trigger('device:resize');
            });
        },

        prepareHotgraphicModel: function() {
          var model = this.model;
          model.set('_component', 'hotgraphic');
          model.set('body', model.get('originalBody'));
          return model;
        },

        animateSliderToIndex: function(itemIndex) {
            var extraMargin = parseInt(this.$('.content-narrative-slider-graphic').css('margin-right')),
                movementSize = this.$('.content-narrative-slide-container').width()+extraMargin;

            this.$('.content-narrative-slider').stop().animate({'margin-left': -(movementSize * itemIndex)});
            this.$('.content-narrative-strapline-header-inner').stop(true, true).animate({'margin-left': -(movementSize * itemIndex)});
        },

        closePopup: function (event) {
            event.preventDefault();
            Adapt.trigger('popup:closed');
            /*this.model.set('_active', true);

            this.$('.content-narrative-popup-close').blur();
            this.$('.content-narrative-popup').addClass('content-narrative-hidden');

            this.evaluateCompletion();*/
        },


        setStage: function(stage) {
            this.model.set('_stage', stage);

            // Set the visited attribute
            var currentItem = this.getCurrentItem(stage);
            currentItem.visited = true;

            this.$('.content-narrative-progress').removeClass('selected');
            this.$('.content-narrative-progress:nth-child('+(stage+1)+')').addClass('selected');
            this.$('.content-narrative-slider-graphic').children('.controls').attr('tabindex', -1);
            this.$('.content-narrative-slider-graphic').eq(stage).children('.controls').attr('tabindex', 0);
            this.$('.content-narrative-content-item').addClass('content-narrative-hidden').eq(stage).removeClass('content-narrative-hidden');

            this.evaluateNavigation();
            this.evaluateCompletion();

            this.animateSliderToIndex(stage);
        },


        constrainStage: function(stage) {
            if (stage > this.model.get('_items').length - 1) {
                stage = this.model.get('_items').length - 1;
            } else if (stage < 0) {
                stage = 0;
            }
            return stage;
        },

        constrainXPosition: function(previousLeft, newLeft, deltaX) {
            if (newLeft > 0 && deltaX > 0) {
                newLeft = previousLeft + (deltaX / (newLeft * 0.1));
            }
            var finalItemLeft = this.model.get('_finalItemLeft');
            if (newLeft < -finalItemLeft && deltaX < 0) {
                var distance = Math.abs(newLeft + finalItemLeft);
                newLeft = previousLeft + (deltaX / (distance * 0.1));
            }
            return newLeft;
        },

        evaluateNavigation: function() {
            var currentStage = this.model.get('_stage');
            var itemCount = this.model.get('_itemCount');

            if (currentStage == 0) {
                this.$('.content-narrative-control-left').addClass('content-narrative-hidden');

                if (itemCount > 1) {
                    this.$('.content-narrative-control-right').removeClass('content-narrative-hidden');
                }
            } else {
                this.$('.content-narrative-control-left').removeClass('content-narrative-hidden');

                if (currentStage == itemCount - 1) {
                    this.$('.content-narrative-control-right').addClass('content-narrative-hidden');
                } else {
                    this.$('.content-narrative-control-right').removeClass('content-narrative-hidden');
                }
            }

        },

        getNearestItemIndex: function() {
            var currentPosition = parseInt(this.$('.content-narrative-slider').css('margin-left')),
                graphicWidth = this.$('.content-narrative-slider-graphic').width(),
                absolutePosition = currentPosition / graphicWidth,
                stage = this.model.get('_stage'),
                relativePosition = stage - Math.abs(absolutePosition);

            if(relativePosition < -0.3) {
                stage++;
            } else if (relativePosition > 0.3) {
                stage--;
            }

            return this.constrainStage(stage);
        },

        getCurrentItem: function(index) {
            return this.model.get('_items')[index];
        },

        getVisitedItems: function() {
          return _.filter(this.model.get('_items'), function(item) {
            return item.visited;
          });
        },

        evaluateCompletion: function() {
            //if (this.getVisitedItems().length == this.model.get('_items').length) {
                this.setCompletionStatus();
            //}
        },

        moveElement: function($element, deltaX) {
            var previousLeft = parseInt($element.css('margin-left')),
                newLeft = previousLeft + deltaX;

            newLeft = this.constrainXPosition(previousLeft, newLeft, deltaX);

            $element.css('margin-left', newLeft + 'px');
        },

        openPopup: function (event) {
            event.preventDefault();
            var currentItem = this.getCurrentItem(this.model.get('_stage')),
                popupObject = {
                    title: currentItem.title,
                    body: currentItem.body
                };

            Adapt.trigger('notify:popup', popupObject);
            Adapt.trigger('popup:opened');
        },

        onNavigationClicked: function(event) {
            event.preventDefault();
            if (!this.model.get('_active')) return;

            var stage = this.model.get('_stage'),
                numberOfItems = this.model.get('_itemCount');

            if ($(event.currentTarget).hasClass('content-narrative-control-right')) {
                stage++;
            } else if ($(event.currentTarget).hasClass('content-narrative-control-left')) {
                stage--;
            }
            stage = (stage + numberOfItems) % numberOfItems;
            this.setStage(stage);
        },

        onTouchNavigationStarted: function(event) {
            //event.preventDefault();
            //if (!this.model.get('_active')) return;

            /*this.$('.content-narrative-slider').stop();
            this.$('.content-narrative-strapline-header-inner').stop();

            this.model.set('_currentX', event.originalEvent.touches[0]['pageX']);
            this.model.set('_touchStartPosition', parseInt(this.$('.content-narrative-slider').css('margin-left')));

            this.$('.content-narrative-slider').on('touchmove', this.onTouchMove);
            this.$('.content-narrative-slider').one('touchend', this.onTouchEnd);*/
        },

        onTouchEnd: function(event) {
            var nextItemIndex = this.getNearestItemIndex();
            this.setStage(nextItemIndex);

            this.$('.content-narrative-slider').off('touchmove', this.onTouchMove);
        },

        onTouchMove: function(event) {
            var currentX = event.originalEvent.touches[0]['pageX'],
                previousX = this.model.get('_currentX'),
                deltaX = currentX - previousX;

            this.moveElement(this.$('.content-narrative-slider'), deltaX);
            this.moveElement(this.$('.content-narrative-strapline-header-inner'), deltaX);

            this.model.set('_currentX', currentX);
            Adapt.trigger('popup:closed');
        }

    });

    Adapt.register("content-narrative", Content_Narrative);

    return Content_Narrative;

});
