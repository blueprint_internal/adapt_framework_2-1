define(function(require) {

    var ComponentView = require('coreViews/componentView');
    var Adapt = require('coreJS/adapt');
    var scormWrapper = require('extensions/adapt-contrib-spoor/js/scorm/wrapper');
    var CoJump = ComponentView.extend({
        events: {
            "click .co-jump-link":"processResult"
            //"click .toggleDrawer":"toggleDrawer"
        },

        preRender: function() {
            this.adapt = Adapt;
            this.model.trackingEventCompleted = false;
            this.$el.addClass("no-state");
            
            // Checks to see if the blank should be reset on revisit
            this.checkIfResetOnRevisit();
            //this.checkCompletion();
            
           
        },

        postRender: function() {
            this.setReadyStatus();
            this.$('.component-inner').on('inview', _.bind(this.inview, this));
            if(this.model.get("_autoComplete")!=undefined)
            {
                this.$('.co-jump_text').css("padding-left",10);
                this.$('.co-jump_text').css("padding-right",10);
            }

        },
        toggleDrawer: function(evt) {
            evt.preventDefault(); 
            this.adapt.trigger('navigation:toggleDrawer');
        },
        
        processResult: function(evt)
        {
            
            if(this.model.get('_autoComplete') == true)
            {
               evt.preventDefault(); 
                
                var scormWrapper = require('extensions/adapt-contrib-spoor/js/scorm/wrapper').getInstance() || null;
                if(scormWrapper && scormWrapper.lmsConnected) {
                    scormWrapper.scorm.data.set("cmi.suspend_data")._isCourseComplete = true
                    scormWrapper.scorm.set("cmi.core.lesson_status", "completed");
                    top.close(); // ONLY works if window was created via script.
                  
                }
            }
            
        },

        // Used to check if the blank should reset on revisit
        checkIfResetOnRevisit: function() {
            var isResetOnRevisit = this.model.get('_isResetOnRevisit');

            // If reset is enabled set defaults
            if (isResetOnRevisit) {
                this.model.reset(isResetOnRevisit);
            }
        },

        inview: function(event, visible, visiblePartX, visiblePartY) {
            if (visible) {
                 
                if (visiblePartY === 'top') {
                    this._isVisibleTop = true;

                } else if (visiblePartY === 'bottom') {
                    this._isVisibleBottom = true;
                } else {
                    this._isVisibleTop = true;
                    this._isVisibleBottom = true;
                }

                if (this._isVisibleTop && this._isVisibleBottom) {

                    this.$('.component-inner').off('inview');
                    this.setCompletionStatus();
                  
                   var parentContentObject = this.model.findAncestor('contentObjects');
                   var parentCompletion = parentContentObject.attributes.completedChildrenAsPercentage;

                   var parentTitle = parentContentObject.attributes.title;
                   var courseTitle = this.adapt.course.attributes.title;
                   if(parentCompletion == 100 && this.model.trackingEventCompleted == false)
                   {
                        this.adapt.trigger('tracking_event',{course:courseTitle+"  module: "+parentTitle,event:"complete"});
                        this.model.trackingEventCompleted = true;
                       
                   }
                }
            }
        },

        checkCompletion: function()
        {
           
            
            if(this.model.get('_postLinkBody') == "")
            {
                var scormWrapper = require('extensions/adapt-contrib-spoor/js/scorm/wrapper').getInstance() || null;
                if(scormWrapper && scormWrapper.lmsConnected) {
                  // Check to see if course was previously completed, if so exit function.
                  var courseStatus = scormWrapper.scorm.get('cmi.core.lesson_status');
                  if(courseStatus.toLowerCase() == "completed")
                {
                    //this.$el.fadeTo()
                }
                else
                {
                    //this.$el
                }
                }
            }
            

        }

    });

    Adapt.register('co-jump', CoJump);

    return CoJump;

});

/*
var scormWrapper = require('extensions/adapt-contrib-spoor/js/scorm/wrapper').getInstance() || null;
    if(scormWrapper && scormWrapper.lmsConnected) {
      // Check to see if course was previously completed, if so exit function.
      var courseStatus = scormWrapper.scorm.get('cmi.core.lesson_status');
      if(courseStatus.toLowerCase() == "completed") return;
    }
*/
/*
checkCompletionStatus: function () {
            // Filter children based upon whether they are available
            var availableChildren = new Backbone.Collection(this.getChildren().where({_isAvailable: true}));
            // Check if any return _isComplete:false
            // If not - set this model to _isComplete: true
            if (availableChildren.findWhere({_isComplete: false, _isOptional: false})) {
                //cascade reset to menu
                this.set({_isComplete:false});
                return;
            }
            this.set({_isComplete: true});
        },
*/
//scorm.set("cmi.core.lesson_status", "completed");