define(function (require) {

    var ComponentView = require('coreViews/componentView');
    var Adapt = require('coreJS/adapt');

    var TextBoxInfo = ComponentView.extend({
        postRender: function () {
            this.division = Math.floor(100 / this.model.get('_items').length);
            this.$('.textbox-info-item').addClass('cell-' + this.division);
            this.setReadyStatus();
        }
    });

    Adapt.register('textbox-info', TextBoxInfo);

});
