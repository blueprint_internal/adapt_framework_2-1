
define(function (require) {

    var ComponentView = require('coreViews/componentView');
    var Adapt = require('coreJS/adapt');

    var WeightScale = ComponentView.extend({
        events: {
            
        },
        postRender: function () {
            this.setReadyStatus();
        }
    });

    Adapt.register('weight-scale', WeightScale);

});
