define(function(require) {

    var ComponentView = require('coreViews/componentView');
    var Adapt = require('coreJS/adapt');
    var ButtonsView = require('coreViews/buttonsView');
    var itemsA;
    var hidddenTextArray = [];
    var abq = false;
    var subj;
    var hiddenTextAccessNum = 0;
    var $selectedElement;
    var TableFormat = ComponentView.extend({

        preRender: function() {
            // Checks to see if the text should be reset on revisit
            this.checkIfResetOnRevisit();
        },



       // events: {
         //   "click .component-inner":"onSubmitClicked"
        //},

         events: {
          //  "click .cell-style-2":"onSubmitClicked"
        },

      
        setupAnimation: function()
        {

///this.model.

            this.cnt = this.model.get("_items").length;
            for(var i=0;i<this.cnt;i++)
            {
               // subj = this.$el.find('#'+i);

                //console.log("subj"+subj);

               /// console.log(Object.keys(subj));

            }


        },
     

        onSubmitClicked: function(event) {


       // var selectedItemObject = this.model.get('_items')[$(event.currentTarget).parent('._cellclass').index;

            //console.log("selectedItemObject" + this.selectedItemObject);

            //debugger

            //var selectedElementA = $(event.target);

            /// console.log("$selectedElement" + Object.keys($selectedElement) );

            //console.log("closest" +  $(event.target).closest("td").index());

            //console.log("this " + this );

            ////$(function(){

            ///$("table tr").click(function(){ alert (this.rowIndex); });

            ///});

            //this.model.get('_items');

            // console.log("remote submission" + evt);

            //console.log("remote submission" + this);

            //var bodytext = this.model.get('body');

            //$("#01").append(hidden_text);

            // $("#01").append(hidddenTextArray[1]);

            //console.log("evt.currentTarget "+Object.keys(evt.currentTarget));

            // console.log("$(this).closest('table') "+ $(this).closest('table').attr('id'))

            // $('td').click(function(){
            //var col = $(this).parent().children().index($(this));
            //ar row = $(this).parent().parent().children().index($(this).parent());
            //alert('Row: ' + row + ', Column: ' + col);
            // });

           //console.log("evt.currentTarget "+Object.keys(evt));

          // $(event.currentTarget).attr(#12);

           this.setupAnimation();

           if( abq === false){

            abq = true;

            //console.log("access_num" + evt.access_num);

                $("#12").append(hidddenTextArray[5]);

                $("#21").append(hidddenTextArray[7]);

                $("#22").append(hidddenTextArray[8]);

                $("#31").append(hidddenTextArray[10]);

                $("#32").append(hidddenTextArray[11]);

                $("#41").append(hidddenTextArray[13]);

            }
            //$("#22").append(hidddenTextArray[8]);

            //this.storeUserAnswer()
            //debugger
            // Used to trigger an event so plugins can display feedback
           // if(this.model.get("_selfSubmitButton")!=undefined){
                //if(this.model.get("_selfSubmitButton")===true){
                   //  console.log("_selfSubmitButton");
                     //this.showFeedback();
              //  }
            //}

        },



        storeHiddenText: function() {

            //console.log("storehiddentext");

            _.each(this.model.get('_items'), function(item, index) {

                var itemRow = (item._row);

                //console.log("itemRow " + itemRow);

                _.each(itemRow, function(rowItem, index) {

                    //console.log("rowItem.hidden_text " + rowItem.hidden_text);

                    hiddenTextAccessNum += 1;

                    console.log("hiddenTextAccessNum" + hiddenTextAccessNum);

                    rowItem.access_num = hiddenTextAccessNum;  

                    console.log("rowItem.access_num" + rowItem.access_num);

                    hidddenTextArray.push(rowItem.hidden_text);

                    //console.log(" hidddenTextArray[x] " + hidddenTextArray[1])

                 },this);

             },this);

              /*  if (item._shouldBeSelected) {
                    numberOfRequiredAnswers ++;

                    if (itemSelected) {
                        numberOfCorrectAnswers ++;
                        
                        item._isCorrect = true;

                        this.model.set('_isAtLeastOneCorrectSelection', true);
                    }

                } else if (!item._shouldBeSelected && itemSelected) {
                    numberOfIncorrectAnswers ++;
                }

            }, this);

            this.model.set('_numberOfCorrectAnswers', numberOfCorrectAnswers);
            this.model.set('_numberOfRequiredAnswers', numberOfRequiredAnswers);

            // Check if correct answers matches correct items and there are no incorrect selections
            var answeredCorrectly = (numberOfCorrectAnswers === numberOfRequiredAnswers) && (numberOfIncorrectAnswers === 0);
            return answeredCorrectly;*/
        },


       postRender: function() {
            this.setReadyStatus();

            // Check if instruction or body is set, otherwise force completion
            var cssSelector = this.$('.component-instruction').length > 0
                ? '.component-instruction'
                : (this.$('.component-body').length > 0 ? '.component-body' : null);
            if (!cssSelector) {
                this.setCompletionStatus();
            } else {
                this.model.set('cssSelector', cssSelector);
                this.$(cssSelector).on('inview', _.bind(this.inview, this));
            }
            this.storeHiddenText();

            this.onSubmitClicked();
        },

        // Used to check if the text should reset on revisit
        checkIfResetOnRevisit: function() {

            //abq = false;

            //var isResetOnRevisit = this.model.get('_isResetOnRevisit');

            // If reset is enabled set defaults
            //if (isResetOnRevisit) {


                //this.model.reset(isResetOnRevisit);
           // }
        },

        inview: function(event, visible, visiblePartX, visiblePartY) {
            if (visible) {


               /// $("#h01").html("Hello jQuery");
                

                if (visiblePartY === 'top') {
                    this._isVisibleTop = true;
                    
                } else if (visiblePartY === 'bottom') {
                    this._isVisibleBottom = true;
                } else {
                    this._isVisibleTop = true;
                    this._isVisibleBottom = true;
                }

                if (this._isVisibleTop && this._isVisibleBottom) {
                    this.$(this.model.get('cssSelector')).off('inview');
                    this.setCompletionStatus();
                }
            }
        }

    });

    Adapt.register('table-format', TableFormat);

    return TableFormat;

});
