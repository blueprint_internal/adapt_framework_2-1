/*
* adapt-survey-radio
* License - http://github.com/adaptlearning/adapt_framework/blob/master/LICENSE
*/
define(function(require) {
	var ComponentView = require('coreViews/componentView');
	var Adapt = require('coreJS/adapt');

	var Component = ComponentView.extend({
		events: function() {
			return {
				"change #item-selector": "processSelection"
			}
		},

		processSelection:function(e){
            this.saveValue(e);
			this.activateDynamicBlocks();
		},

		activateDynamicBlocks: function() {
			var dynamicBlocks = this.model.get('_dynamicBlocks');
			if( typeof dynamicBlocks === 'object' ){
				var items = this.model.get('_items');
				var userSelection = this.model.get('userSelection');
				if(userSelection === 'random') userSelection = GetRandomCase(items); // This function prevents updating the model
				DisableBlock(dynamicBlocks); // Disable all blocks
				EnableBlock(dynamicBlocks, userSelection); // Enable only the blocks selected
			}

			function GetRandomCase(items){
				// Items containing objects with value 'random' will cause this function to continue
				var selection = 'random';
				while(selection === 'random') {
					var min = 0;
					var max = items.length;
					var rng = Math.floor(Math.random() * max) + min;
					selection = items[rng].value;
				}
				return selection;
			}

			function EnableBlock(dynamicBlocks, userSelection){
				$.each(dynamicBlocks, function(key, value){
					if( userSelection === key ){
						for(var i = 0; i < value.length; i++) {
                            
							var caseStudyComponents = Adapt.components.findWhere({_parentId:value[i]});
                            if(caseStudyComponents!=undefined)
                            {
                                caseStudyComponents.attributes._isAvailable = true;
                            }
							
						}
					}
				});
			}

			function DisableBlock(dynamicBlocks){
                
				$.each(dynamicBlocks, function(key, value){
					for(var i = 0; i < value.length; i++) {
						var caseStudyComponents = Adapt.components.findWhere({_parentId:value[i]});
                        if(caseStudyComponents!=undefined)
                        {
                            if(caseStudyComponents.attributes!= undefined){caseStudyComponents.attributes._isAvailable = false;};
                        }
						
					}
				});
                
			}
		},

		saveValue: function(e) {
			var element = e.target;
            this.model.set('userSelection', element.value);
			this.setSelection();
			Adapt.trigger("survey:refresh", this);
            //this.$('.component-body').css("zoom","100%")
            document.body.style.zoom=1;
            
            //document.body.style.z-index
            //window.innerWidth=screen.width
		},

		setSelection: function() {
			var selection = this.model.get('userSelection');
			var element = this.el.getElementsByTagName('input');
			for(var i = 0; i < element.length; i++) {
				$(element[i].parentNode).removeClass("selected");
				if(element[i].value === selection) {
					element[i].checked = true;
					$(element[i].parentNode).addClass("selected");
				}
			}
		},

		validateSelection: function() {
			var selection = this.model.get('userSelection');
			if(typeof selection === "object") {
				selection = selection[0];
				this.model.set('userSelection', selection);
			}
		},

		preRender: function() {
			this.validateSelection();
		},

		postRender: function() {
			this.setReadyStatus();

			// Select items currently set on model for userSelection
			this.setSelection();

			// Check if instruction or body is set, otherwise force completion
			var cssSelector = this.$('.component-instruction').length > 0 ? '.component-instruction'
				: (this.$('.component-body').length > 0 ? '.component-body' : null);

			if (!cssSelector) {
				this.setCompletionStatus();
			} else {
				this.model.set('cssSelector', cssSelector);
				this.$(cssSelector).on('inview', _.bind(this.inview, this));
			}
		},

		inview: function(event, visible, visiblePartX, visiblePartY) {
			if (visible) {
				if (visiblePartY === 'top') {
					this._isVisibleTop = true;
				} else if (visiblePartY === 'bottom') {
					this._isVisibleBottom = true;
				} else {
					this._isVisibleTop = true;
					this._isVisibleBottom = true;
				}

				if (this._isVisibleTop && this._isVisibleBottom) {
					this.$(this.model.get('cssSelector')).off('inview');
					this.setCompletionStatus();
				}
			}
		}
	});
	Adapt.register("survey-radio-dropdown", Component);
});