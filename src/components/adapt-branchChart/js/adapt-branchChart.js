/*
* adapt-branchChart
* License - http://github.com/adaptlearning/adapt_framework/LICENSE
* Maintainers - Sean Callahan <callahancg@gmail.com>
*/

define(function(require) {

    var ComponentView = require('coreViews/componentView');
    var Adapt = require('coreJS/adapt');
    var feedback = [];
	var branchChart = ComponentView.extend({

		events: {
            "click .branchChart-text":"launchFb"
		},


        preRender: function() {
            //this.getFeedback();
        },

        postRender: function() {
            this.setReadyStatus();
            this.$('.component-inner').on('inview', _.bind(this.inview, this));
            this.hideEmptySlots();
            this.setBorderColor();
            //if (this.model.get("_feedback")) $('.branchChart-text').css('cursor', 'pointer');
            this.normalizeTextRowHeights();
        },

        // Used to check if the blank should reset on revisit
        checkIfResetOnRevisit: function() {
            var isResetOnRevisit = this.model.get('_isResetOnRevisit');

            // If reset is enabled set defaults
            if (isResetOnRevisit) {
                this.model.reset(isResetOnRevisit);
            }
        },

        inview: function(event, visible, visiblePartX, visiblePartY) {
            if (visible) {
                if (visiblePartY === 'top') {
                    this._isVisibleTop = true;
                } else if (visiblePartY === 'bottom') {
                    this._isVisibleBottom = true;
                } else {
                    this._isVisibleTop = true;
                    this._isVisibleBottom = true;
                }

                if (this._isVisibleTop && this._isVisibleBottom) {
                    this.$('.component-inner').off('inview');
                    this.setCompletionStatus();
                }

            }
        },

        getFeedback: function(elementId) {
            var feedbackObj = this.model.get("_feedback");

            for (var key in feedbackObj) {
              if (feedbackObj.hasOwnProperty(key)) {
                if (key === elementId) return feedbackObj[key];
              }
            }
        },

        getTitle: function(elementId) {
            var feedbackTitleObj = this.model.get("_title");

            for (var key in feedbackTitleObj) {
              if (feedbackTitleObj.hasOwnProperty(key)) {
                if (key === elementId) return feedbackTitleObj[key];
              }
            }
        },

        launchFb: function(event) {
            if (event) event.preventDefault();
            // Get ID of clicked element as Int to call feedback from target array
            var elementId = $(event.currentTarget).attr('id');
            var fbMsg = this.getFeedback(elementId);
            var fbTitle = this.getTitle(elementId);
            if (fbMsg && fbTitle) {
                Adapt.trigger('notify:popup', {
                   title: fbTitle,
                   body: fbMsg
                });
            }
        },

        normalizeTextRowHeights: function() {
            function resizeText() {
                /* Second Row */
                var midLeftTextHeight = this.$('.branchChart-midLeftText').height();
                var midRightTextHeight = this.$('.branchChart-midRightText').height();

                var midTextMax = Math.max(midLeftTextHeight, midRightTextHeight);
                this.$('.branchChart-midRightText').css('height', midTextMax + 'px');
                this.$('.branchChart-midLeftText').css('height', midTextMax + 'px');
                //console.log('Max ' + max)

                /* Third Row */
                var bottomLeftOuterTextHeight = this.$('#bottomLeftInnerFb').height();
                var bottomLeftInnerTextHeight = this.$('#bottomLeftOuterFb').height();
                var bottomRightInnerTextHeight = this.$('#bottomRightInnerFb').height();
                var bottomRightOuterTextHeight = this.$('#bottomRightOuterFb').height();

                var bottomTextMax = Math.max(bottomLeftOuterTextHeight, bottomLeftInnerTextHeight, bottomRightInnerTextHeight, bottomRightOuterTextHeight);
                this.$('#bottomLeftInnerFb').css('height', bottomTextMax + 'px');
                this.$('#bottomLeftOuterFb').css('height', bottomTextMax + 'px');
                this.$('#bottomRightInnerFb').css('height', bottomTextMax + 'px');
                this.$('#bottomRightOuterFb').css('height', bottomTextMax + 'px');
                console.log('bottomTextMax ' + bottomTextMax);

                /* Fourth Row */
                var lastRowLeftOuterTextHeight = this.$('#lastRowLeftOuterFb').height();
                var lastRowLeftInnerTextHeight = this.$('#lastRowLeftInnerFb').height();
                var lastRowRightInnerTextHeight = this.$('#lastRowRightInnerFb').height();
                var lastRowRightOuterTextHeight = this.$('#lastRowRightOuterFb').height();

                var lastRowTextMax = Math.max(lastRowLeftOuterTextHeight, lastRowLeftInnerTextHeight, lastRowRightInnerTextHeight, lastRowRightOuterTextHeight);
                this.$('#lastRowLeftOuterFb').css('height', lastRowTextMax + 'px');
                this.$('#lastRowLeftInnerFb').css('height', lastRowTextMax + 'px');
                this.$('#lastRowRightInnerFb').css('height', lastRowTextMax + 'px');
                this.$('#lastRowRightOuterFb').css('height', lastRowTextMax + 'px');
                console.log('lastRowTextMax ' + lastRowTextMax);
            }

            setTimeout(function() {
                resizeText();
            },1000);
        },

        hideEmptySlots: function() {
            var bottomLeftOuterText = this.model.get('bottomLeftOuterText');
            var bottomLeftInnerText = this.model.get('bottomLeftInnerText');
            var bottomRightInnerText = this.model.get('bottomRightInnerText');
            var bottomRightOuterText = this.model.get('bottomRightOuterText');
            var lastRowLeftOuterText = this.model.get('lastRowLeftOuterText');
            var lastRowLeftInnerText = this.model.get('lastRowLeftInnerText');
            var lastRowRightInnerText = this.model.get('lastRowRightInnerText');
            var lastRowRightOuterText = this.model.get('lastRowRightOuterText');

            if (!bottomLeftOuterText) {
                //console.log('no bottomLeftOuterText')
                this.$('.branchChart-textDividerLeft> .branchChart-bottomLineLeft').css('visibility', 'hidden');
                $('.branchChart-textDividerLeft> .branchChart-bottomLeftText').css('visibility', 'hidden');
            }
            if (!bottomLeftInnerText) {
                //console.log('no bottomLeftInnerText')
                this.$('.branchChart-textDividerLeft> .branchChart-bottomLineRight').css('visibility', 'hidden');
                $('.branchChart-textDividerLeft> .branchChart-bottomRightText').css('visibility', 'hidden');
            } else if(!bottomLeftOuterText) {
                this.$('.branchChart-textDividerLeft> .branchChart-bottomLineRight').addClass('branchChart-bottomLineRight-topLine');
                this.$('.branchChart-textDividerLeft> .branchChart-bottomLineRight').removeClass('branchChart-bottomLineRight');
            }

            if (!bottomRightInnerText) {
                //console.log('no bottomRightInnerText')
                this.$('.branchChart-textDividerRight> .branchChart-bottomLineLeft').css('visibility', 'hidden');
                $('.branchChart-textDividerRight> .branchChart-bottomLeftText').css('visibility', 'hidden');
            }
            if (!bottomRightOuterText) {
                //console.log('no bottomRightOuterText')
                this.$('.branchChart-textDividerRight> .branchChart-bottomLineRight').css('visibility', 'hidden');
                $('.branchChart-textDividerRight> .branchChart-bottomRightText').css('visibility', 'hidden');
            } else if(!bottomRightInnerText) {
                this.$('.branchChart-textDividerRight> .branchChart-bottomLineRight').addClass('branchChart-bottomLineRight-topLine');
                this.$('.branchChart-textDividerRight> .branchChart-bottomLeftText').removeClass('branchChart-bottomLineRight');
            }
            if (!lastRowLeftOuterText) {
                console.log('no lastRowLeftOuterText')
                this.$('.branchChart-textDividerLeft> .branchChart-lastRow-line-left').css('visibility', 'hidden');
                $('.branchChart-textDividerLeft> #lastRowLeftOuterFb').css('visibility', 'hidden');
            }
            if (!lastRowLeftInnerText) {
                console.log('no lastRowLeftInnerText')
                this.$('.branchChart-textDividerLeft> .branchChart-lastRow-line-right').css('visibility', 'hidden');
                $('.branchChart-textDividerLeft> #lastRowLeftInnerFb').css('visibility', 'hidden');
            }
            if (!lastRowRightInnerText) {
                console.log('no lastRowRightInnerText')
                this.$('.branchChart-textDividerRight> .branchChart-lastRow-line-left').css('visibility', 'hidden');
                $('.branchChart-textDividerRight> #lastRowRightInnerFb').css('visibility', 'hidden');
            }
            if (!lastRowRightOuterText) {
                console.log('no lastRowRightOuterText')
                this.$('.branchChart-textDividerRight> .branchChart-lastRow-line-right').css('visibility', 'hidden');
                $('.branchChart-textDividerRight> #lastRowRightOuterFb').css('visibility', 'hidden');
            }
        },

        setBorderColor: function() {
            var borderColor = this.model.get('borderColor');
            if (borderColor) {
                $('.branchChart-caption').css('border-color', borderColor);
                //$('.branchChart-lineBottom').css('border-color', borderColor);
            }
        }

    });

    Adapt.register("branchChart", branchChart);
	return branchChart;

});
