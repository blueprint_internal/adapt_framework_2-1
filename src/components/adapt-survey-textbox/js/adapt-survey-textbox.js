/*
* adapt-survey-textbox
* License - http://github.com/adaptlearning/adapt_framework/blob/master/LICENSE
*/
define(function(require) {
	var ComponentView = require('coreViews/componentView');
	var Adapt = require('coreJS/adapt');

	var Component = ComponentView.extend({
		events: function() {
			return {
				'keyup .survey-textbox-item-input':'saveValue'
			}
		},

		saveValue: function(e) {
			var element = e.target;
			this.model.set('userSelection', element.value);
			Adapt.trigger("survey:refresh", this);
		},

		postRender: function() {
			this.setReadyStatus();

			// Check if instruction or body is set, otherwise force completion
			var cssSelector = this.$('.component-instruction').length > 0 ? '.component-instruction'
				: (this.$('.component-body').length > 0 ? '.component-body' : null);

			if (!cssSelector) {
				this.setCompletionStatus();
			} else {
				this.model.set('cssSelector', cssSelector);
				this.$(cssSelector).on('inview', _.bind(this.inview, this));
			}
		},

		inview: function(event, visible, visiblePartX, visiblePartY) {
			if (visible) {
				if (visiblePartY === 'top') {
					this._isVisibleTop = true;
				} else if (visiblePartY === 'bottom') {
					this._isVisibleBottom = true;
				} else {
					this._isVisibleTop = true;
					this._isVisibleBottom = true;
				}

				if (this._isVisibleTop && this._isVisibleBottom) {
					this.$(this.model.get('cssSelector')).off('inview');
					this.setCompletionStatus();
				}
			}
		}
	});
	Adapt.register("survey-textbox", Component);
});