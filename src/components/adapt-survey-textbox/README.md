#adapt-survey-textbox

This component provides a text entry field where any value can be entered.


##Installation

Clone the repo into your project:
        [project_name]\src\components

##Survey Components

Although these components look and function similar to existing components (textInput, mcq or gmcq) they inherit directly from the component class; this is different than standard question components which inherit from the question class and ties into the tutor system. Also unique to these survey components is the ability to detect content changes and store the new value automatically, and the ability to maintain state when navigating to different pages, articles, and components.

##Attributes

####userSelection (string)

The user's entered value; this can be set beforehand.

####prefix (string) (HTML enabled)

A textual area preceding the input field.

####suffix (string) (HTML enabled)

A textual area following the input field.

##Example

See the example.json for structuring.

##Known Problems

None at this time.

